#### Background

I create laravel applications very often. Therefore I want to increase my productivity by auto generating the important, but boring aspects of my Laravel appz. The code is a bit messy, but I strive to make improvements. I have tested it on existing medium to large applications without any problems

#### What this package will generate (via `php artisan`)
* Models with `belongsTo` `belongsToMany` and `hasMany` relations (Optionally with policies and observers). These are generated using your current connection.
* Factories with `belongsTo` relations and `Faker` . These are generated using your existing models.
* Repositories (Repository pattern). These are generated using your existing models.
* Controllers with Routes and Requests (Optionally with the usage of policies). 
* Unit tests. These are generated from your defined routes at `api.php`

#### okkk Show me some generated code, cowboy!

[Here is an example of what will be generated](docs/example.md)

### Getting started
> This package requires php 7.2 and was built using Laravel 6.0 but might work in 5.5+ with some tweaks
>
* install with composer `composer require tarre/laravel-kickstarter --dev`
* publish config `php artisan vendor:publish --tag=laravel-kickstarter`
* look thru and adjust `config\laravel-kickstarter.php` to your liking before starting.
* Check out the [documentation](docs/README.md) for more info

#### All available commands _(Cheat sheet)_
* `kickstart:models {--create-observers} {--create-policies} {--ide-helper} {--connection=} {--overwrite} {--ignore-providers} {--filter=.*}`
* `kickstart:factories {--overwrite} {--filter=.*}`
* `kickstart:repositories {--overwrite} {--ignore-providers} {--filter=.*}`
* `kickstart:controllers {--use-policies} {--overwrite} {--filter=.*}`
* `kickstart:tests {--overwrite} {--filter=.*}`

