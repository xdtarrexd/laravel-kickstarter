<?php


namespace Tarre\Kickstarter;


use Doctrine\DBAL\Schema\Column;
use Faker\Factory;

class ValueRetriever
{
    const FOR_REQUEST = 0;
    const FOR_TEST = 1;
    const FOR_FACTORY = 2;

    public static function ForFactoryFromColumn(Column $column, array $relations)
    {
        $colName = $column->getName();
        $colType = (string)$column->getType();
        // Se if this is a belongsTo method. Then we return a factory closure
        if (isset($relations[$colName])) {
            return sprintf('factory:%s', $relations[$colName]);
        }

        // "parse no brainers"
        if (preg_match('/bool/i', $colType)) {
            return '$faker->boolean';
        }

        // get default value before wild goose chasing
        if (!is_null($column->getDefault())) {
            return is_numeric($column->getDefault()) ? $column->getDefault() : "'" . $column->getDefault() . "'";
        }

        // Attempt to figure out some common things before resorting to types
        if (preg_match('/^first.*name/i', $colName)) {
            return '$faker->firstName';
        }

        if (preg_match('/^last.*name/i', $colName)) {
            return '$faker->lastName';
        }

        if (preg_match('/user|account/i', $colName)) {
            return '$faker->userName';
        }

        if (preg_match('/name/i', $colName)) {
            return '$faker->name';
        }

        if (preg_match('/mail/i', $colName)) {
            return '$faker->safeEmail';
        }

        if (preg_match('/address/i', $colName)) {
            return '$faker->streetAddress';
        }

        if (preg_match('/uid/i', $colName)) {
            return '$faker->uuid';
        }

        if (preg_match('/city/i', $colName)) {
            return '$faker->city';
        }

        if (preg_match('/title/i', $colName)) {
            return '$faker->title';
        }

        if (preg_match('/numb|phone/i', $colName)) {
            return '$faker->e164PhoneNumber';
        }


        // "last resorts before resorting to null values"
        if (preg_match('/blob|binary/i', $colType)) {
            return 'null';
        }

        if (preg_match('/json/i', $colType)) {
            return '[]';
        }

        if (preg_match('/int/i', $colType)) {
            return '$faker->randomNumber()';
        }

        if (preg_match('/float/i', $colType)) {
            return '$faker->randomFloat()';
        }

        if (preg_match('/datetime/i', $colType)) {
            return '$faker->datetime()';
        }

        if (preg_match('/date/i', $colType)) {
            return '$faker->date()';
        }

        if (preg_match('/time/i', $colType)) {
            return '$faker->time()';
        }

        if (preg_match('/string/i', $colType)) {
            return '$faker->word';
        }

        if (preg_match('/text/i', $colType)) {
            return '$faker->text';
        }

        // We failed.
        return 'null';
    }

    public static function ForRequestsFromColumn(Column $column): array
    {
        $rules = [];
        if ($column->getNotnull()) {
            $rules[] = 'required';
        } else {
            $rules[] = 'nullable';
        }

        if (preg_match('/mail/i', $column->getName())) {
            $rules[] = 'email';
        }

        if (preg_match('/json/i', $column->getType())) {
            $rules[] = 'array';
        }

        if (preg_match('/boolean|smallint/i', $column->getType())) {
            $rules[] = 'boolean';
        }

        if (preg_match('/int/i', $column->getType())) {
            $rules[] = 'numeric';
        }

        if ($column->getUnsigned()) {
            $rules[] = 'min:0';
        }

        /*
        if (preg_match('/string/i', $column->getType())) {
            $rules[] = 'string';
        }
        */

        if (preg_match('/date/i', $column->getType())) {
            $rules[] = 'date';
        }

        return $rules;
    }

    public static function ForTesFromMixed(string $name, array $rules)
    {

        // anything that ends with "id" we will just null, because we cannot know atm.
        if (preg_match('/id$/i', $name)) {
            return 'null';
        }

        // anything that ends with "_at" we treat as a date
        if (preg_match('/_at$/i', $name)) {
            return "'" . now() . "'";
        }

        $faker = Factory::create();
        // FYI: Strings has to be returned with quotes
        if (in_array('numeric', $rules)) {
            return $faker->randomNumber();
        }

        if (in_array('email', $rules)) {
            return "'" . $faker->safeEmail . "'";
        }

        if (in_array('array', $rules)) {
            return '[]';
        }

        if (in_array('boolean', $rules)) {
            return 'true';
        }

        // Educated guesses
        if (preg_match('/^first.*name/i', $name)) {
            return "'" . $faker->firstName . "'";
        }

        if (preg_match('/^last.*name/i', $name)) {
            return "'" . $faker->lastName . "'";
        }

        if (preg_match('/user|account/i', $name)) {
            return "'" . $faker->userName . "'";
        }

        if (preg_match('/name/i', $name)) {
            return "'" . $faker->name . "'";
        }

        if (preg_match('/mail/i', $name)) {
            return "'" . $faker->safeEmail . "'";
        }

        if (preg_match('/address/i', $name)) {
            return "'" . $faker->streetAddress . "'";
        }

        if (preg_match('/uid/i', $name)) {
            return "'" . $faker->uuid . "'";
        }

        if (preg_match('/city/i', $name)) {
            return "'" . $faker->city . "'";
        }

        if (preg_match('/title/i', $name)) {
            return "'" . $faker->title . "'";
        }

        if (preg_match('/numb|phone/i', $name)) {
            return "'" . $faker->e164PhoneNumber . "'";
        }

        if (preg_match('/text|desc/i', $name)) {
            return "'" . $faker->text . "'";
        }

        if (preg_match('/payload|array/i', $name)) {
            return "'[]'";
        }

        if (preg_match('/date/i', $name)) {
            return "'" . now() . "'";
        }

        if (in_array('string', $rules)) {
            return "'" . $faker->word . "'";
        }

        if (in_array('required', $rules)) {
            return "'required value, unknown type'";
        }

        return "''"; // empty
    }

}
