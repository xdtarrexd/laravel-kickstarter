<?php


namespace Tarre\Kickstarter\Generators\Factory;


use Doctrine\DBAL\Schema\Column;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;
use ReflectionClass;
use Tarre\Kickstarter\Helper;
use Tarre\Kickstarter\ValueRetriever;

class FactoryStructureDetectiveLol
{

    public function resolve(ReflectionClass $reflectionClass, array $connections)
    {
        $modelName = $reflectionClass->getName();
        if ((new \ReflectionClass($modelName))->isAbstract()) {
            return null;
        }
        $model = new $modelName;

        if (method_exists($model, 'getConnectionName') && !is_null($model->getConnectionName())) {
            if (!in_array($model->getConnectionName(), $connections)) {
                return null;
            }
        }

        $relations = $this->getRelations($reflectionClass, $model);
        $attributes = $this->getAttributes($model, $relations);

        return [
            'className' => $reflectionClass->getName(),
            'attributes' => $attributes->toArray()
        ];
    }

    /**
     * @param Model $model
     * @param array $relations
     * @return Collection
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function getAttributes(Model $model, array $relations): Collection
    {
        $schemaManager = $model->getConnection()->getDoctrineSchemaManager();
        $platform = $schemaManager->getDatabasePlatform();
        Helper::polyFillDoctrineTypes($platform);

        return collect($schemaManager
            ->listTableColumns($model->getTable()))
            ->filter(function (Column $column) use ($model) {
                // Ignore models prim key
                if ($column->getName() == $model->getKeyName()) {
                    return false;
                }
                // ignore deleted at if SoftDeletes are enabled
                if (method_exists($model, 'getDeletedAtColumn')) {
                    if ($model->getDeletedAtColumn() == $column->getName()) {
                        return false;
                    }
                }
                return true;
            })
            ->mapWithKeys(function (Column $column) use ($relations) {
                // attributeName => print value
                return [
                    $column->getName() => ValueRetriever::ForFactoryFromColumn($column, $relations)
                ];
            });
    }

    protected function getRelations(ReflectionClass $reflectionClass, Model $model): array
    {
        // Grab the content of our model. This will not care for extensions or traits.
        $modelFileContent = file_get_contents($reflectionClass->getFileName());

        // See if we have any functions to checkout!
        if (preg_match_all('/function\h+\s*([a-z]+[a-z_0-9]*)\h*\(\h*\)/mi', $modelFileContent, $methodsToCheckOut)) {
            $methodsToCheckOut = $methodsToCheckOut[1];
        } else {
            return [];
        }

        // Map relation foreign keys to its relative. So we can use fact those aswell
        return collect($methodsToCheckOut)
            ->filter(function ($methodName) use ($model) {
                // Filter out anything that is not an belongsTo relation
                try {
                    return $model->$methodName() instanceof BelongsTo;
                } catch (\Exception $exception) {
                    return false;
                }
            })
            ->mapWithKeys(function ($methodName) use ($model) {
                $belongsToRelation = $model->$methodName();

                return [
                    $belongsToRelation->getForeignKeyName() => get_class($belongsToRelation->getRelated())
                ];
            })->toArray();
    }

    public function getAttributeValue(Column $column, array $relations): string
    {

    }
}
