<?php


namespace Tarre\Kickstarter\Generators\Factory;


use Faker\Generator;
use Illuminate\Support\Str;
use Nette\PhpGenerator\PhpFile;

class FactoryGenerator
{
    protected $pair = [];

    public function __construct(array $pair)
    {
        $this->pair = $pair;
    }

    public function getSavePath(): string
    {
        return base_path() . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . 'factories' . DIRECTORY_SEPARATOR . $this->getClassBaseName() . 'Factory.php';
    }

    public function getContent(): string
    {
        // create initial file
        $file = (New PhpFile())
            ->addComment('@var \Illuminate\Database\Eloquent\Factory $factory')
            ->addUse(Generator::class, 'Faker')
            ->addUse($this->getClassName());

        // create content
        $body = [];
        $body[] = sprintf("\$factory->define(%s::class, function (Faker \$faker) {", $this->getClassBaseName());
        $body[] = '    return [';

        $body[] = collect($this->getAttributes())
            ->map(function ($attributeValue, $attributeName) use ($file) {
                $array = [];

                if (Str::startsWith($attributeValue, 'factory:')) {

                    $modelName = Str::after($attributeValue, 'factory:');
                    $file->addUse($modelName);

                    $array[] = sprintf("        '%s' => function () {", $attributeName);
                    $array[] = sprintf("            return factory(%s::class)->create()->id;", class_basename($modelName)); // TODO: settingize the ID
                    $array[] = sprintf("        },");
                } else {
                    $array[] = sprintf("        '%s' => %s,", $attributeName, $attributeValue);
                }

                return $array;
            })
            ->flatten(1)
            ->implode(PHP_EOL);

        $body[] = '    ];';
        $body[] = '});';

        // voila
        return $file . PHP_EOL . implode(PHP_EOL, $body);
    }

    protected function getClassName(): string
    {
        return $this->pair['className'];
    }

    protected function getClassBaseName(): string
    {
        return class_basename($this->getClassName());
    }

    protected function getAttributes()
    {
        return $this->pair['attributes'];
    }

}
