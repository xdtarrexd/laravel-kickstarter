<?php


namespace Tarre\Kickstarter\Generators\Test;


use Exception;
use Faker\Factory;
use Illuminate\Support\Collection;
use InvalidArgumentException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionException;
use ReflectionParameter;
use Tarre\Kickstarter\KS\Method;
use Tarre\Kickstarter\ValueRetriever;

/**
 * @property Route route
 */
class TestGeneratorMethodStubs
{

    protected $route;

    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    public function getSavePath(): string
    {
        return base_path('tests') . DIRECTORY_SEPARATOR . 'Feature' . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getClassname(): string
    {
        return Str::Studly($this->getControllerClassBaseName() . 'Test');
    }

    public function getMethods(): array
    {
        $baseName = 'test_' . Str::snake($this->getRoute()->getActionMethod());

        // Try to catch request attributes
        try {
            $attributes = $this->getControllerRequestAttributes();
        } catch (Exception $exception) {
            $attributes = [];
        }
        // because we collect
        $attributes = collect($attributes);

        $methodA = $this->makeTest($baseName . '_with_failure', $attributes, true);
        $methodB = $this->makeTest($baseName . '_with_success', $attributes, false);

        // Ignore fail test with index
        if ($this->getRoute()->getActionMethod() == 'index') {
            return [$methodB];
        }

        return [$methodA, $methodB];
    }

    public function getRoute(): Route
    {
        return $this->route;
    }

    protected function makeTest($name, Collection $attributes, $fail = false): Method
    {
        $method = new Method($name);

        $jsonMethod = $this->getJsonMethod();

        // add setupUser
        $method->addBody('$this->setupUser();');
        $method->addBody('');

        // att test lock
        /*
        $method->setBody([
            'echo "Validate test and remove these 2 lines";',
            'return;'
        ]);
        */
        $parsedUri = preg_replace('/\{\w+\}/', '%s', $this->getRoute()->uri());

        if (in_array($jsonMethod, ['GET', 'DELETE'])) {
            $method->addBody(
                sprintf("\$response = \$this->json('%s', config('app.url') . sprintf('/%s'));", $this->getJsonMethod(), $parsedUri)
            );
        } else {
            $method->addBody(
                sprintf("\$response = \$this->json('%s', config('app.url') . sprintf('/%s'), [", $this->getJsonMethod(), $parsedUri)
            );

            // if we do not want to fail. provide some attributes
            if (!$fail) {
                $attributes
                    ->each(function ($value, $name) use ($method) {
                        $method->addBody(sprintf("    '%s' => %s,", $name, $value));
                    });
            }

            $method->addBody(']);');
        }

        $method->addBody('');

        // determine how to assert
        if (in_array($jsonMethod, ['GET', 'DELETE'])) {
            if ($fail) {
                $method->addBody("\$response->assertStatus(Response::HTTP_NOT_FOUND);");
            } else {
                $method->addBody("\$response->assertStatus(Response::HTTP_OK);");
            }
        } else {
            if ($fail) {
                $method->addBody("\$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);");
            } else {
                if ($jsonMethod == 'POST') {
                    $method->addBody("\$response->assertStatus(Response::HTTP_CREATED);");
                } else {
                    $method->addBody("\$response->assertStatus(Response::HTTP_OK);");
                }
            }
        }


        return $method;
    }

    public function getControllerClassBaseName(): string
    {
        return class_basename($this->getControllerClass());
    }

    public function getControllerClass(): string
    {
        return (string)get_class($this->getRoute()->getController());
    }

    /**
     * @return string
     */
    protected function getJsonMethod(): string
    {
        $routeMethods = $this->getRoute()->methods();

        if (in_array('GET', $routeMethods) || in_array('ANY', $routeMethods)) {
            return 'GET';
        }

        if (in_array('PUT', $routeMethods)) {
            return 'PUT';
        }

        if (in_array('PATCH', $routeMethods)) {
            return 'PATCH';
        }

        if (in_array('POST', $routeMethods)) {
            return 'POST';
        }

        if (in_array('DELETE', $routeMethods)) {
            return 'DELETE';
        }

        throw new InvalidArgumentException('WTF cannot resolve any method. Am i stupid?');
    }

    /**
     * @return array
     * @throws ReflectionException
     */
    protected function getControllerRequestAttributes()
    {
        $controller = $this->getRoute()->getController();
        $controllerReflection = new ReflectionClass($controller);
        $controllerReflectionMethod = $controllerReflection->getMethod($this->getRoute()->getActionMethod());
        $reflectionParameters = $controllerReflectionMethod->getParameters();

        // Grab first attribute that is an instance of an FormRequest
        $attributes = collect($reflectionParameters)
            ->map(function (ReflectionParameter $parameter) {
                // ignore non type hints
                if (!$parameter->hasType()) {
                    return null;
                }
                // ignore if the type hinted parameter is not an class
                if (is_null($parameter->getClass())) {
                    return null;
                }
                // inspect the class and see if its an request
                $className = $parameter->getClass()->getName();
                $class = new $className;

                if (!$class instanceof FormRequest) {
                    return null;
                }
                // skip if the rules method is missing
                if (!method_exists($class, 'rules')) {
                    return null;
                }

                $filledAttributes = collect($class->rules())->mapWithKeys(function ($rules, $name) {
                    // RequestRules can sometimes be string piped instead of arrayable. So we need to pipe->array them
                    if (!is_array($rules)) {
                        $rules = explode('|', $rules);
                    }
                    // Sometimes rules can also be passed with an Illuminate\Validation\Rule. Example (Rule::in('a', 'b', 'c'))
                    // We can just class_basename these to get the real names
                    $rules = collect($rules)
                        ->map(function ($rule) {
                            if (is_object($rule)) {
                                return strtolower(class_basename(get_class($rule)));
                            }
                            return $rule;
                        })->toArray();

                    // Map attribute
                    return [
                        $name => ValueRetriever::ForTesFromMixed($name, $rules)
                    ];
                });

                return $filledAttributes;
            })->reject(null)->first();

        return $attributes;
    }


}
