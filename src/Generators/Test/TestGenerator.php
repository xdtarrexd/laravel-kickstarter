<?php


namespace Tarre\Kickstarter\Generators\Test;


use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tests\TestCase;


class TestGenerator extends KSBase
{
    protected $methods;
    protected $path;

    public function __construct(array $methods, string $path)
    {
        $this->methods = $methods;
        $this->path = $path;

        $this->addUses(TestCase::class);
        $this->addUses(RefreshDatabase::class);
        $this->addUses(Response::class);
        $this->addTrait(RefreshDatabase::class);
    }

    public function extends(): array
    {
        return [
            TestCase::class
        ];
    }

    public function getSavePath(): string
    {
        return base_path('tests') . DIRECTORY_SEPARATOR . 'Feature' . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        return 'Tests\Feature';
    }

    public function getClassname(): string
    {
        $base = base_path('tests');
        $relative = str_replace($base, '', $this->path);
        $baseName = basename($relative, '.php');
        return $baseName;
    }

    public function getMethods(): array
    {
        return $this->methods;
    }

}
