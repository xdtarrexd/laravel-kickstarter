<?php


namespace Tarre\Kickstarter\Generators\Repository;


use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tarre\Kickstarter\KS\Method;


/**
 * @property Collection repositories
 */
class ServiceProviderGenerator extends KSBase
{

    protected $repositories;

    public function __construct(Collection $repositories)
    {
        $this->repositories = $repositories;

        $this->setUses([
            ServiceProvider::class
        ]);

        $this->repositories->each(function ($pair) {
            $this->addUses($pair['interface']);
            $this->addUses($pair['class']);
        });
    }

    public function extends(): array
    {
        return [
            ServiceProvider::class
        ];
    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.repositories.serviceProvider_path');
    }

    public function getNamespace(): string
    {
        return config('laravel-kickstarter.repositories.serviceProvider_namespace');
    }

    public function getClassname(): string
    {
        return 'RepositoryProvider'; // TODO add to config
    }

    public function getMethods(): array
    {
        return [
            $this->getMethodRegister()
        ];
    }

    protected function getMethodRegister()
    {
        $method = new Method('register');

        $this->repositories->each(function ($pair) use ($method) {
            $method->addBody(sprintf("\$this->app->bind(%s::class, %s::class);",
                class_basename($pair['interface']),
                class_basename($pair['class'])));
        });

        return $method;
    }
}
