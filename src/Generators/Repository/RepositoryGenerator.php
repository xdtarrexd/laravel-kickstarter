<?php


namespace Tarre\Kickstarter\Generators\Repository;


use Illuminate\Support\Str;
use Tarre\Kickstarter\KS\Abstracts\KSBase;

/**
 * @property EloquentGenerator eloquent
 */
class RepositoryGenerator extends KSBase
{
    protected $eloquent;

    public function __construct(EloquentGenerator $eloquentGenerator)
    {
        // We will build the interface using everything the Eloquent has to offer. Minus the actual body
        $this->eloquent = $eloquentGenerator;
        $this->setUses($this->eloquent->getUses());
    }

    public function isInterface(): bool
    {
        return true;
    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.repositories.directory') . DIRECTORY_SEPARATOR . $this->eloquent->getModelBaseName() . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        $namespace = str_replace(app_path(), '', config('laravel-kickstarter.repositories.directory'));
        $namespace = sprintf('App%s\\%s', $namespace, $this->eloquent->getModelBaseName());
        $namespace = str_replace('/', '\\', $namespace);
        return $namespace;
    }

    public function getClassname(): string
    {
        return Str::studly($this->eloquent->getModelBaseName() . 'Repository');
    }

    public function getMethods(): array
    {
        return $this->eloquent->getMethods();
    }

}
