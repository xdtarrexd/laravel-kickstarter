<?php


namespace Tarre\Kickstarter\Generators\Repository;


use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionException;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tarre\Kickstarter\KS\Argument;
use Tarre\Kickstarter\KS\Interfaces\KsMethod;
use Tarre\Kickstarter\KS\Method;

/**
 * @property ReflectionClass modelReflection
 * @property \Tarre\Kickstarter\KS\Interfaces\KsFile interface
 */
class EloquentGenerator extends KSBase
{
    protected $model;
    protected $interface;
    protected $modelReflection;

    /**
     * EloquentGenerator constructor.
     * @param string $model
     * @throws ReflectionException
     */
    public function __construct(string $model)
    {
        $this->model = $model;
        $this->modelReflection = new ReflectionClass($model);
        /*
         * Set usages
         */
        $this->setUses([
            LengthAwarePaginator::class,
            $model,
            Exception::class,
            // $this->interface->getNamespaceClassname() // May not be visible since Nette php will see they live in the same space
        ]);

        /*
         * Add the interface as an dependency, which also depends on this class. Lol
         */
        $this->interface = $this->addDependencyFile('interface', new RepositoryGenerator($this));

    }

    public function implements(): array
    {
        return [
            $this->interface->getNamespaceClassname()
        ];
    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.repositories.directory') . DIRECTORY_SEPARATOR . $this->getModelBaseName() . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        $namespace = str_replace(app_path(), '', config('laravel-kickstarter.repositories.directory'));
        $namespace = sprintf('App%s\\%s', $namespace, $this->getModelBaseName());
        $namespace = str_replace('/', '\\', $namespace);
        return $namespace;
    }

    public function getClassname(): string
    {
        return Str::studly('Eloquent' . $this->getModelBaseName());
    }

    public function getMethods(): array
    {
        $methods = [
            $this->getMethodIndex(),
            $this->getMethodFindOrFail(),
            $this->getMethodStore(),
            $this->getMethodUpdate(),
            $this->getMethodDestroyOrFail()
        ];

        foreach ($this->getMethodCustom() as $method) {
            $methods[] = $method;
        }

        return $methods;
    }

    /*
     * All Models
     */

    protected function getMethodIndex(): KsMethod
    {
        $index = new Method('index');

        $index->addComment(sprintf('@return %s', class_basename(LengthAwarePaginator::class)));
        $index->setType(LengthAwarePaginator::class);

        $index->addArgument((new Argument('search'))->setDefault(''));
        $index->addArgument((new Argument('filter'))->setDefault('null'));

        $index->setBody([
            'if (empty($search)) {',
            sprintf('    return %s::paginate();', $this->getModelBaseName()),
            '}',
            sprintf('return %s::search($search)->paginate()->appends(\'search\', $search);', $this->getModelBaseName())
        ]);

        return $index;
    }

    protected function getMethodFindOrFail(): KsMethod
    {
        $findOrFail = new Method('findOrFail');

        $findOrFail->addComment(sprintf('@return %s', $this->getModelBaseName()));
        $findOrFail->setType($this->getModel());

        $findOrFail->addArgument(new Argument($this->getModelIdAttributeName()));

        $findOrFail->addBody(sprintf('return %s::whereId($%s)->firstOrFail();', $this->getModelBaseName(), $this->getModelIdAttributeName()));

        return $findOrFail;
    }


    protected function getMethodStore(): KsMethod
    {
        $store = new Method('store');

        $store->addComment(sprintf('@return %s', $this->getModelBaseName()));
        $store->setType($this->getModel());

        $store->addArgument((new Argument('attributes'))->setType('array'));

        $store->addBody(sprintf('$%s = new %s;', $this->getModelAttributeBaseName(), $this->getModelBaseName()));
        $store->addBody(sprintf('$%s->fill($attributes);', $this->getModelAttributeBaseName()));
        $store->addBody(sprintf('$%s->save();', $this->getModelAttributeBaseName()));
        $store->addBody(sprintf('return $%s;', $this->getModelAttributeBaseName()));

        return $store;
    }

    protected function getMethodUpdate(): KsMethod
    {
        $update = new Method('update');

        $update->addComment(sprintf('@return %s', $this->getModelBaseName()));
        $update->setType($this->getModel());

        $update->addArgument((new Argument('attributes'))->setType('array'));
        $update->addArgument(new Argument($this->getModelIdAttributeName()));

        $update->addBody(sprintf('$%s = $this->findOrFail($%s);', $this->getModelAttributeBaseName(), $this->getModelIdAttributeName()));
        $update->addBody(sprintf('$%s->fill($attributes);', $this->getModelAttributeBaseName()));
        $update->addBody(sprintf('$%s->save();', $this->getModelAttributeBaseName()));
        $update->addBody(sprintf('return $%s;', $this->getModelAttributeBaseName()));

        return $update;
    }


    protected function getMethodDestroyOrFail(): KsMethod
    {
        $destroyOrFail = new Method('destroyOrFail');

        $destroyOrFail->addComment(sprintf('@return %s', $this->getModelBaseName()));
        $destroyOrFail->addComment(sprintf('@throws %s', Exception::class));
        $destroyOrFail->setType($this->getModel());

        $destroyOrFail->addArgument(new Argument($this->getModelIdAttributeName()));
        $destroyOrFail->addArgument((new Argument('forceDelete'))->setDefault(false));

        $destroyOrFail->addBody(sprintf('$%s = $this->findOrFail($%s);', $this->getModelAttributeBaseName(), $this->getModelIdAttributeName()));
        $destroyOrFail->addBody('if ($forceDelete) {');
        $destroyOrFail->addBody(sprintf('    $%s->forceDelete();', $this->getModelAttributeBaseName()));
        $destroyOrFail->addBody('} else {');
        $destroyOrFail->addBody(sprintf('    $%s->delete();', $this->getModelAttributeBaseName()));
        $destroyOrFail->addBody('}');
        $destroyOrFail->addBody(sprintf('return $%s;', $this->getModelAttributeBaseName()));

        return $destroyOrFail;
    }

    protected function getMethodCustom(): array
    {
        $contents = file_get_contents($this->getModelReflectionClass()->getFileName());

        if (preg_match_all('/\$this->(?:belongsToMany|hasMany)\(["\']?([a-z0-9_\\\\]+)/mi', $contents, $m)) {
            return collect(data_get($m, 1))->map(function ($relatedClass) {

                $relatedClassBaseName = class_basename($relatedClass);
                $relatedClassNamePluralized = Str::plural($relatedClassBaseName);
                $methodName = Str::camel('get' . $relatedClassNamePluralized);
                $classNameCamel = $this->getModelAttributeBaseName();

                $method = new Method($methodName);

                $method->addComment(sprintf('@return %s', class_basename(LengthAwarePaginator::class)));

                $method->setType(LengthAwarePaginator::class);

                $method->addArgument(new Argument($this->getModelIdAttributeName()));
                $method->addArgument((new Argument('search'))->setDefault(''));
                $method->addArgument((new Argument('filter'))->setDefault('null'));

                $method->setBody([
                    sprintf('$%s = $this->findOrFail($%s);', $classNameCamel, $this->getModelIdAttributeName()),
                    //'if (empty($search)) {',
                    //'//' . $relatedClass. '::class',
                    //'}',
                    sprintf("return $%s->%s()->paginate()->appends('search', \$search);", $classNameCamel, $relatedClassNamePluralized)
                ]);

                return $method;
            })->toArray();
        }

        return [];
    }


    /*
     * Helpers
     */

    protected function getModel(): string
    {
        return $this->model;
    }

    /**
     * This function is used at the REpository Creator
     * @return string
     */
    public function getModelBaseName(): string
    {
        return class_basename($this->getModel());
    }

    protected function getModelIdAttributeName(): string
    {
        return $this->getModelAttributeBaseName() . 'Id';
    }

    protected function getModelAttributeBaseName(): string
    {
        return Str::camel($this->getModelBaseName());
    }

    protected function getModelReflectionClass(): ReflectionClass
    {
        return $this->modelReflection;

    }
}
