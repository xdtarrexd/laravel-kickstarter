<?php


namespace Tarre\Kickstarter\Generators\Controller;


use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tarre\Kickstarter\KS\Argument;
use Tarre\Kickstarter\KS\Property;
use Tarre\Kickstarter\KS\Interfaces\KsMethod;
use Tarre\Kickstarter\KS\Method;

/**
 * @property ReflectionClass $repository
 */
class ControllerGenerator extends KSBase
{
    /**
     * @var bool
     */
    private $policiesEnabled;

    public function __construct(ReflectionClass $repository, $policiesEnabled = false)
    {
        /*
          * Initial setup for controller
          */
        $this->repository = $repository;
        $this->policiesEnabled = $policiesEnabled;

        /*
         * Add comments
         */
        $this->setComments([
            sprintf('@property %s %s', class_basename($this->getRepositoryClassName()), $this->getRepositoryClassNameAttribute())
        ]);

        /*
         * Create a request file for the store and update method
         */
        $request = $this->addDependencyFile('request', new RequestGenerator($this->guessModelNameFromRepositoryName()));

        /*
         * Set usages
         */

        $this->setUses([
            LengthAwarePaginator::class,
            Request::class,
            Exception::class,
            $request->getNamespaceClassname(),
            $this->getRepositoryClassName(),
            $this->guessModelFQDNFromRepositoryName()
        ]);

        /*
         * add more usages if we are using auth
         */
        if ($this->usePolicies()) {
            $this->addUses(AuthorizationException::class);
        }
    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.controllers.directory') . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        return config('laravel-kickstarter.controllers.namespace');
    }

    public function extends(): array
    {
        return [
            Controller::class
        ];
    }


    public function getClassname(): string
    {
        return sprintf('%sController', $this->guessModelNameFromRepositoryName());
    }

    public function getProperties(): array
    {
        return [
            (new Property($this->getRepositoryClassNameAttribute()))->setVisibility('protected')
        ];
    }

    public function getMethods(): array
    {
        $methods = [
            $this->getMethodConstruct(),
            $this->getMethodIndex(),
            $this->getMethodShow(),
            $this->getMethodStore(),
            $this->getMethodUpdate(),
            $this->getMethodDestroy()
        ];

        // What methods we want to ignore when getting other misc
        $ignoreTheseMethodNames = collect($methods)
            ->map(function (KsMethod $ksMethod) {
                return $ksMethod->getName();
            })
            ->merge([ // TODO: Because the method name is "show" and the repository name is "findOrFail" we have to manually take that into account
                'findOrFail',
                'destroyOrFail'
            ])
            ->toArray();

        // this will create getter methods for all other methods in
        collect($this->repository->getMethods())
            ->filter(function (ReflectionMethod $reflectionMethod) use ($ignoreTheseMethodNames) {
                // ignore already created methods
                return !in_array($reflectionMethod->getName(), $ignoreTheseMethodNames);
            })
            ->each(function (ReflectionMethod $reflectionMethod) use (&$methods) {
                $methods[] = $this->getMethodCustom($reflectionMethod);
            });

        return $methods;
    }

    // Methods

    protected function getMethodConstruct(): KsMethod
    {
        $construct = new Method('__construct');

        $repository = new Argument($this->getRepositoryClassNameAttribute());

        $repository->setType($this->getRepositoryClassName());

        $construct->addArgument($repository);

        $construct->addBody(sprintf("\$this->%s = $%s;", $this->getRepositoryClassNameAttribute(), $this->getRepositoryClassNameAttribute()));

        return $construct;
    }

    protected function getMethodIndex(): KsMethod
    {
        $index = new Method('index');

        if ($this->usePolicies()) {
            $index->addBody(sprintf("\$this->authorize('viewAny', %s::class);", $this->guessModelNameFromRepositoryName()));
            $index->addComment(sprintf('@throws %s', class_basename(AuthorizationException::class)));
        }
        $index->addComment(sprintf('@return %s', class_basename(LengthAwarePaginator::class)));

        $index->addBody(sprintf("\$search = Request::get('search', '');"));
        $index->addBody(sprintf("\$filter = Request::get('filter');"));
        $index->addBody(sprintf('return $this->%s->index($search, $filter);', $this->getRepositoryClassNameAttribute()));

        return $index;
    }

    protected function getMethodShow(): KsMethod
    {
        $show = new Method('show');

        $argumentId = (new Argument('id'));

        $show->addArgument($argumentId);

        $show->addBody(sprintf('$%s = $this->%s->findOrFail($%s);', $this->getMethodBodyAttributeName(), $this->getRepositoryClassNameAttribute(), $argumentId->getName()));

        if ($this->usePolicies()) {
            $show->addBody(sprintf("\$this->authorize('view', $%s);", $this->getMethodBodyAttributeName()));
            $show->addComment(sprintf('@throws %s', class_basename(AuthorizationException::class)));
        }
        $show->addComment(sprintf('@return %s', $this->guessModelNameFromRepositoryName()));

        $show->addBody(sprintf('return $%s;', $this->getMethodBodyAttributeName()));

        return $show;
    }

    protected function getMethodStore(): KsMethod
    {
        $request = $this->getDependencyFile('request');

        $store = new Method('store');

        $request = (new Argument('request'))->setType($request->getNamespaceClassname());

        $store->addArgument($request);

        if ($this->usePolicies()) {
            $store->addBody(sprintf("\$this->authorize('create', %s::class);", $this->guessModelNameFromRepositoryName()));
            $store->addComment(sprintf('@throws %s', class_basename(AuthorizationException::class)));
        }
        $store->addComment(sprintf('@return %s', $this->guessModelNameFromRepositoryName()));

        $store->addBody(sprintf('return $this->%s->store($request->all());', $this->getRepositoryClassNameAttribute()));


        return $store;
    }

    protected function getMethodUpdate(): KsMethod
    {
        $request = $this->getDependencyFile('request');

        $argumentId = (new Argument('id'));

        $update = new Method('update');

        $request = (new Argument('request'))->setType($request->getNamespaceClassname());

        $update->addArgument($request);
        $update->addArgument($argumentId);

        if ($this->usePolicies()) {
            $update->addBody(sprintf('$%s = $this->%s->findOrFail($%s);', $this->getMethodBodyAttributeName(), $this->getRepositoryClassNameAttribute(), $argumentId->getName()));
            $update->addBody(sprintf("\$this->authorize('update', $%s);", $this->getMethodBodyAttributeName()));
            $update->addComment(sprintf('@throws %s', class_basename(AuthorizationException::class)));
        }
        $update->addComment(sprintf('@return %s', $this->guessModelNameFromRepositoryName()));

        $update->addBody(sprintf('return $this->%s->update($request->all(), $id);', $this->getRepositoryClassNameAttribute()));

        return $update;
    }

    protected function getMethodDestroy(): KsMethod
    {
        $destroy = new Method('destroy');

        $argumentId = (new Argument('id'));

        $destroy->addArgument($argumentId);

        if ($this->usePolicies()) {
            $destroy->addBody(sprintf('$%s = $this->%s->findOrFail($%s);', $this->getMethodBodyAttributeName(), $this->getRepositoryClassNameAttribute(), $argumentId->getName()));
            $destroy->addBody(sprintf("\$this->authorize('delete', $%s);", $this->getMethodBodyAttributeName()));
            $destroy->addComment(sprintf('@throws %s', class_basename(AuthorizationException::class)));
        }
        $destroy->addComment(sprintf('@throws %s', class_basename(Exception::class)));
        $destroy->addComment(sprintf('@return %s', $this->guessModelNameFromRepositoryName()));

        $destroy->addBody(sprintf('return $this->%s->destroyOrFail($id);', $this->getRepositoryClassNameAttribute()));

        return $destroy;
    }

    protected function getMethodCustom(ReflectionMethod $reflectionMethod): Method
    {
        $method = new Method($reflectionMethod->getName());

        // Find out if we have some sort of primary ID situation goin on here
        $primaryIdParam = collect($reflectionMethod->getParameters())
            ->filter(function (ReflectionParameter $parameter) {
                return preg_match('/id/i', $parameter->getName());
            })->first();

        // get everything not related to primary id we fetch with getz
        $requestGets = collect($reflectionMethod->getParameters())
            ->filter(function (ReflectionParameter $parameter) {
                return !preg_match('/id/i', $parameter->getName());
            });


        // if we have a primaryIdparam we make sure its the first id we use. This is also a requirement for Policies to "work"
        if ($primaryIdParam) {
            $argumentId = new Argument($primaryIdParam->getName());
            $method->addArgument($argumentId);
            if ($this->usePolicies()) {
                $method->addBody(sprintf('$%s = $this->%s->findOrFail($%s);', $this->getMethodBodyAttributeName(), $this->getRepositoryClassNameAttribute(), $argumentId->getName()));
                $method->addBody(sprintf("\$this->authorize('view', $%s);", $this->getMethodBodyAttributeName()));
                $method->addComment(sprintf('@throws %s', class_basename(AuthorizationException::class)));
                $method->addComment(sprintf('@return %s', class_basename(LengthAwarePaginator::class)));
            }
        }

        // add request get attributes
        collect($requestGets)
            ->each(function (ReflectionParameter $parameter) use ($method) {
                try {
                    if (is_array($parameter->getDefaultValue())) {
                        $default = '[]';
                    } else if (is_numeric($parameter->getDefaultValue())) {
                        $default = $parameter->getDefaultValue();
                    } else if (!is_null($parameter->getDefaultValue())) {
                        $default = "'" . str_replace("'", "\'", $parameter->getDefaultValue()) . "'";
                    } else {
                        $default = 'null';
                    }
                } catch (\ReflectionException $exception) {
                    // this probably occurs when no optional was set
                    $default = 'null';
                }

                $method->addBody(sprintf("\$%s = Request::get('%s', %s);", $parameter->getName(), $parameter->getName(), $default));

            });

        // create our callString that we pass to our return statment
        $callString =
            collect([$primaryIdParam])
                ->merge($requestGets)
                ->reject(null)
                ->map(function (ReflectionParameter $parameter) {
                    return sprintf('$%s', $parameter->getName());
                })->implode(', ');

        $method->addBody(sprintf('return $this->%s->%s(%s);', $this->getRepositoryClassNameAttribute(), $reflectionMethod->getName(), $callString));
        return $method;
    }


    /*
     * "helpers""
     */

    protected function getRepositoryClassNameAttribute(): string
    {
        return Str::camel(class_basename($this->getRepositoryClassName()));
    }

    protected function getRepositoryClassName(): string
    {
        return $this->repository->getName();
    }

    protected function getMethodBodyAttributeName(): string
    {
        return Str::camel($this->guessModelNameFromRepositoryName());
    }

    /**
     * NOTE: This method expects the repository interface to be formatted as "ClassNameRepository". Example If repository is named "CompanyRepository" the model is "Company"
     *
     * @return string
     */
    public function guessModelNameFromRepositoryName(): string
    {
        return preg_replace('/repository$/i', '', class_basename($this->repository->getName()));
    }

    protected function guessModelFQDNFromRepositoryName(): string
    {
        return config('laravel-kickstarter.models.namespace') . '\\' . $this->guessModelNameFromRepositoryName();
    }

    protected function usePolicies()
    {
        return $this->policiesEnabled;
    }

}
