<?php


namespace Tarre\Kickstarter\Generators\Controller;


use Doctrine\DBAL\Schema\Column;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Collection;
use Tarre\Kickstarter\Helper;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tarre\Kickstarter\KS\Method;
use Tarre\Kickstarter\ValueRetriever;

class RequestGenerator extends KSBase
{
    private $modelClassName;

    public function __construct(string $modelName)
    {
        $this->modelClassName = $modelName;
        $this->addUses(FormRequest::class);
    }

    public function extends(): array
    {
        return [FormRequest::class];
    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.controllers.requests.directory') . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        return config('laravel-kickstarter.controllers.requests.namespace');
    }

    public function getClassname(): string
    {
        return sprintf('Store%sRequest', $this->modelClassName);
    }

    public function getMethods(): array
    {
        $authorize = new Method('authorize');
        $authorize->addComment('Determine if the user is authorized to make this request.' . PHP_EOL);
        $authorize->addComment('@return bool');
        $authorize->addBody('return true;');

        $rule = new Method('rules');
        $rule->addComment('Get the validation rules that apply to the request.' . PHP_EOL);
        $rule->addComment('@return array');

        // Create ruleset (guessing game)
        $ruleSet = $this->generateRulesetArray();

        // Add rules to body
        $rule->addBody(sprintf('return [%s];', $this->generateRulesetFromArrayAsString($ruleSet)));

        // Do same for array
        $messages = new Method('messages');
        $messages->addBody('// Remove comments to return a custom message');
        $messages->addBody(sprintf('return [%s];', $this->generateRulesetMessagesFromArrayAsString($ruleSet)));

        // Return methods
        return [
            $authorize,
            $rule,
            $messages
        ];
    }

    protected function generateRulesetFromArrayAsString(array $ruleset): string
    {
        $ruleAttributeString = collect($ruleset)
            ->map(function ($rules, $name) {
                $rules = collect($rules)
                    ->map(function ($rule) {
                        return sprintf("'%s'", $rule);
                    })->implode(', ');

                return sprintf("    '%s' => [%s]", $name, $rules);
            })
            ->values()
            ->implode(',' . PHP_EOL);

        return PHP_EOL . $ruleAttributeString . PHP_EOL;
    }

    protected function generateRulesetMessagesFromArrayAsString(array $ruleset): string
    {
        $ruleAttributeString = collect($ruleset)
            ->map(function ($rules, $name) {
                return collect($rules)
                    ->filter(function ($rule) {
                        return !in_array($rule, $this->rulesToIgnoreInMessages());
                    })
                    ->map(function ($rule) use ($name) {
                        return sprintf("    //'%s.%s' => ''", $name, $rule);
                    })->join(',' . PHP_EOL);
            })
            ->values()
            ->reject('') // reject empty objects in "rulesToignoreInMessages
            ->implode(',' . PHP_EOL);

        return PHP_EOL . $ruleAttributeString . PHP_EOL;
    }

    protected function generateRulesetArray(): array
    {
        // In order to return our rules. We need to see what the database contains (if found)
        $model = $this
            ->getModels()
            ->filter(function (string $model) {
                return $this->modelClassName == class_basename($model);
            })->first();
        if ($model && !(new \ReflectionClass($model))->isAbstract()) {
            $model = new $model;
        } else {
            $model = null;
        }

        if ($model) {
            // Use doctrine to figure out what to put in our ruleset
            $ruleAttributes = (function (Model $model) {
                $attributes = $model->getConnection()
                    ->getDoctrineSchemaManager()
                    ->listTableColumns($model->getTable());

                // Ignore these model attributes
                $ignoreThese = array_merge($model->getDates(), [$model->getKeyName(), 'deleted_at']);

                return collect($attributes)
                    ->filter(function (Column $column) use ($ignoreThese) {
                        return !in_array($column->getName(), $ignoreThese);
                    })
                    ->mapWithKeys(function (Column $column) {
                        return [
                            $column->getName() => ValueRetriever::ForRequestsFromColumn($column)
                        ];
                    })->toArray();
            })($model); // CALCULATE AND RETURN
        } else {
            $ruleAttributes = []; // Could not calculate model. Empty ruleset.
        }

        return $ruleAttributes;
    }

    protected function rulesToIgnoreInMessages(): array
    {
        return [
            'nullable'
        ];
    }

    protected function getModels(): Collection
    {
        static $models;
        if (!$models) {
            $models = Helper::findClassThatExtends(Model::class);
        }

        return $models;
    }


}
