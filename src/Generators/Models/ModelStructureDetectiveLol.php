<?php


namespace Tarre\Kickstarter\Generators\Models;


use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\ForeignKeyConstraint;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Tarre\Kickstarter\Helper;

class ModelStructureDetectiveLol
{
    protected $connection;
    protected $pivotClasses;

    public function __construct($connection)
    {
        $this->connection = $connection;
        $this->pivotClasses = [];
    }

    /**
     * @return Collection
     * @throws DBALException
     */
    public function getClasses()
    {
        // Get tables with information like foreign keys, attributes etc.
        $tables = Helper::getConnectionTables($this->connection);

        // Create pivot class pairs for filter stage.
        $classPairs = $this->buildPivotClassPairs($tables);

        $tables = $tables
            ->map(function ($table) {
                return $this->preBuildStage($table);
            })
            ->filter(function ($table) use ($classPairs) {
                return $this->filterStage($table, $classPairs);
            });

        // Builds base classes, common attributes, belongsto, belongstomany and hasmany relationship
        return $tables->map(function ($table) use ($tables) {
            return $this->postBuildStage($table, $tables);
        });
    }

    public function preBuildStage(array $table)
    {
        /*
        |--------------------------------------------------------------------------
        | Prebuild stage
        |--------------------------------------------------------------------------
        |
        | This stage crates the initial data that does not require knowing the end result:
        |  - Attribute fillable
        |  - SoftDeletes
        |  - BelongsTo relation
        |  - Attribute casting
        */

        return $this->buildInitialClass($table);
    }

    protected function filterStage($table, Collection $classPairs)
    {

        // Look for pairs in our previously created classPairs collection
        $classPair = $classPairs->where('id', $table['className']);

        // If it was a hit. We save the table className to our pivotClasses and filter it from the $tables collection
        if ($classPair->count() == 1) {
            $this->pivotClasses[] = $classPair->first()['classNames'];
            return false;
        } else {
            return true;
        }
    }

    public function postBuildStage($table, collection $tables)
    {
        /*
        |--------------------------------------------------------------------------
        | Post build stage
        |--------------------------------------------------------------------------
        |
        | This stage finishes the last parts of the class:
        | - inversed BelongsTo relations
        | -

        */

        return $this->postBuildClass($table, $tables);
    }

    protected function buildInitialClass($table)
    {
        $fillable = $table['columns']->keys()->filter(function ($key) {
            return !in_array($key, ['id', 'created_at', 'updated_at', 'deleted_at']);
        })
            ->sort()
            ->values()
            ->toArray();

        // determine if we use soft deletes
        $softDeletes = $table['columns']->has('deleted_at');

        // determine if notifiable trait may be added
        $notifiable = count(preg_grep('/mail|phone/', $fillable)) > 0; // $fillable here has no actual reason but DRY

        // Determine all belongsTo relations
        $belongsToRelations = $table['foreignKeys']
            ->map(function (ForeignKeyConstraint $constraint) {
                $foreignClassId = $constraint->getColumns()[0];
                $foreignTableName = $constraint->getForeignTableName();
                $foreignClassName = Str::studly(Str::singular($foreignTableName));
                return [
                    'foreign_id' => $foreignClassId,
                    'name' => $foreignClassName
                ];
            })
            ->toArray();
        // Cast types
        $casts = $table['columns']
            ->filter(function ($column, $key) {
                return !in_array($key, ['id', 'created_at', 'updated_at', 'deleted_at']);
            })
            ->map(function (Column $column) {
                $type = (string)$column->getType();
                $default = (string)$column->getDefault();

                // Match any strings that have defaulted for "json"
                if (preg_match('/^\h*(?:{.*}|\[.*])\h*$/', $default)) {
                    return 'array';
                }

                switch ($type) {
                    case '\BigInt':
                    case '\Int':
                        return 'int';
                    case '\Boolean':
                        return 'boolean';
                    case '\String':
                        // case '\Text':
                        return 'string';
                    case '\Json':
                        return 'array';
                }
                return null;
            })
            ->reject(null)
            ->sortKeys()
            ->toArray();

        // hidden
        $hidden = $table['columns']
            ->filter(function (Column $column) {
                return preg_match('/binary|blob/i', (string)$column->getType());
            })
            ->map(function (Column $column) {
                return $column->getName();
            })->values()->toArray();

        // guess primary id by giving scores by clues and then taking the highest scoring
        $primaryKey = $table['columns']
            ->map(function (Column $column) {
                $score = 0;
                // exact match of ID is considered very good
                if (preg_match('/^id$/i', $column->getName())) {
                    $score += 7;
                    // loose match of ID is ok. But it also requires the autoIncrement to win over this ID
                } elseif (preg_match('/id/i', $column->getName())) {
                    $score += 3;
                }
                if ($column->getAutoincrement()) {
                    $score += 6;
                }
                return [
                    'name' => $column->getName(),
                    'score' => $score
                ];
            })
            ->sortByDesc('score')
            ->first();

        if ($primaryKey) {
            $primaryKey = $primaryKey['name'];
        } else {
            $primaryKey = null;
        }

        return [
            'primaryKey' => $primaryKey,
            'tableName' => $table['tableName'],
            'className' => $table['className'],
            'fillable' => $fillable,
            'notifiable' => $notifiable,
            'softDeletes' => $softDeletes,
            'belongsTo' => $belongsToRelations,
            'casts' => $casts,
            // These attributes are filled in the next stage
            'hasMany' => [],
            'belongsToMany' => [],
            'hasManyThrough' => [],
            'hidden' => $hidden,
            'appends' => []
        ];
    }

    protected function postBuildClass($table, Collection $tables)
    {
        // Every belongsTo relation has been created. That means we can now look for the "opposite" of belongsTo which is "hasMany"
        $hasMany = $tables->filter(function ($subTable) use ($table) {
            return in_array($table['className'], collect($subTable['belongsTo'])->pluck('name')->toArray());
        })
            ->map(function ($subTable) {
                return [
                    'foreign_id' => data_get($subTable, 'belongsTo.0.foreign_id'),
                    'name' => $subTable['className']
                ];
            })
            ->values()
            ->toArray();

        // check the pivotClasses and see if we have any belongsToMany relations we can add
        $belongsToMany = collect($this->pivotClasses)
            ->filter(function ($pair) use ($table) {
                // First check if we are found in any pair
                return in_array($table['className'], $pair);
            })
            ->map(function ($pair) use ($table) {
                // A model cant have an belongsToMany relationship with itself.
                return collect($pair)
                    ->filter(function ($p) use ($table) {
                        return $p !== $table['className'];
                    })->first();
            })
            // Cleanup
            ->flatten(1)
            ->toArray();

        // create singular variants of hasMany and belongsToMany since they will just pluck the className which is in (most cases) plural
        $hasManyPluralForHidden = collect($hasMany)
            ->map(function ($value) {
                return Str::plural($value['name']);
            })->toArray();

        $belongsToManyForHidden = collect($belongsToMany)
            ->map(function ($value) {
                return Str::plural($value);
            })->toArray();

        $belongsToForHidden = collect($table['belongsTo'])
            ->map(function ($belongsTo) {
                return $belongsTo['name'];
            })->toArray();

        // Create hidden
        $hidden = array_merge($table['hidden'], $belongsToForHidden, $hasManyPluralForHidden, $belongsToManyForHidden);

        // Also hide passwords
        if (in_array('password', $table['fillable'])) {
            $hidden[] = 'password';
        }

         // create getters for belongsTo relationship attributes
        $appends = collect($table['belongsTo'])->map(function ($belongsToClass) use ($tables) {
            $relatedClass = $tables->where('className', $belongsToClass['name'])->first();
            
            return collect($relatedClass['fillable'])->map(function ($attribute) use ($belongsToClass, $relatedClass) {

                // figure out default values based on how the relatedClass casts the attributes
                // Its dangerous to play with bool and ints
                // So for now we could stick with strings and arrays
                $default = 'null';
                if (isset($relatedClass['casts'][$attribute])) {
                    switch ($relatedClass['casts'][$attribute]) {
                        case 'string':
                            $default = "''";
                            break;
                        case 'array':
                            $default = '[]';
                            break;
                    }
                }

                // create the getter_name
                $getter = Str::studly($belongsToClass['name']) . Str::studly(ucfirst($attribute));
                return [
                    'getter' => sprintf('get%sAttribute', $getter),
                    'className' => $belongsToClass['name'],
                    'attribute' => $attribute,
                    'appendName' => Str::snake($getter),
                    'default' => $default
                ];
            })->toArray();
        })
            ->flatten(1)
            ->toArray();


        return
            array_merge(
                $table,
                [
                    'hasMany' => $hasMany,
                    'belongsToMany' => $belongsToMany,
                    'hidden' => collect($hidden)->sort()->values()->toArray(),
                    'appends' => collect($appends)->sort()->values()->toArray()
                ]
            );
    }

    /**
     * This will create every pivot possibility of all table Classes (Company + Role) = CompanyRole etc..
     * @param Collection $tables
     * @return Collection
     */
    protected function buildPivotClassPairs(Collection $tables)
    {
        return $tables->map(function ($tableA) use ($tables) {
            return collect($tables)
                ->filter(function ($tableB) use ($tableA) {
                    // Ignore pairs of self
                    return $tableA['className'] !== $tableB['className'];
                })
                ->map(function ($tableB) use ($tableA) {
                    // Create pairs
                    $pairs = [$tableA['className'], $tableB['className']];
                    // Asc
                    sort($pairs);
                    return [
                        'id' => implode('', $pairs),
                        'classNames' => $pairs
                    ];
                });
        })
            ->flatten(1)
            ->unique('id');// easier to debug
    }
}
