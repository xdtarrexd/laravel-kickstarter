<?php


namespace Tarre\Kickstarter\Generators\Models;


use Illuminate\Support\Str;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tarre\Kickstarter\KS\Argument;
use Tarre\Kickstarter\KS\Method;

/**
 * @property ModelGenerator model
 */
class ObserverGenerator extends KSBase
{
    protected $model;

    public function __construct(ModelGenerator $modelGenerator)
    {
        $this->model = $modelGenerator;

        $this->addUses($this->model->getNamespaceClassname());
    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.models.observers.directory') . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        return config('laravel-kickstarter.models.observers.namespace');
    }

    public function getClassname(): string
    {
        return $this->getModel()->getClassname() . 'Observer';
    }

    public function getMethods(): array
    {
        return collect(config('laravel-kickstarter.models.observers.events'))
            ->map(function ($event) {
                $camelModelName = Str::camel($this->getModel()->getClassname());
                $method = new Method($event);
                $method
                    ->addComment(sprintf('Handle the %s "%s" event', $camelModelName, $event))
                    ->addComment('');
                //->setType($this->model->getNamespaceClassname());
                $method->addBody('//');
                $method->addArgument((new Argument($camelModelName))->setType($this->getModel()->getNamespaceClassname()));

                return $method;
            })->toArray();
    }

    public function getModel(): ModelGenerator
    {
        return $this->model;
    }
}
