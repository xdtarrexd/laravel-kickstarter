<?php


namespace Tarre\Kickstarter\Generators\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tarre\Kickstarter\KS\Method;
use Tarre\Kickstarter\KS\Property;

class ModelGenerator extends KSBase
{
    protected $metaData;

    public function __construct(array $metaData)
    {
        $this->metaData = $metaData;

        /*
         * Add database traits
         */
        if ($metaData['notifiable']) {
            $this->addUses(Notifiable::class);
            $this->addTrait(Notifiable::class);
        }

        if ($metaData['softDeletes']) {
            $this->addUses(SoftDeletes::class);
            $this->addTrait(SoftDeletes::class);
        }


        foreach(config('laravel-kickstarter.models.extends') as $item){
            $this->addUses($item);
        }

    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.models.directory') . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        return config('laravel-kickstarter.models.namespace');
    }

    public function getClassname(): string
    {
        return $this->metaData['className'];
    }

    public function extends(): array
    {
        return config('laravel-kickstarter.models.extends');
    }

    public function getProperties(): array
    {
        $properties = [];

        $properties[] = (new Property('table'))->setValue($this->metaData['tableName']);
        $properties[] = (new Property('primaryKey'))->setValue($this->metaData['primaryKey']);
        $properties[] = (new Property('fillable'))->setValue($this->metaData['fillable']);
        $properties[] = (new Property('casts'))->setValue($this->metaData['casts']);
        $properties[] = (new Property('hidden'))->setValue($this->metaData['hidden']);
        $properties[] = (new Property('appends'))->setValue(collect($this->metaData['appends'])->pluck('appendName')->toArray());

        return $properties;
    }

    public function getMethods(): array
    {
        $methods = [];

        // belongsToMany
        collect($this->metaData['belongsToMany'])
            ->each(function ($relationShip) use (& $methods) {
                $method = new Method(Str::plural($relationShip));
                $method->addBody(sprintf('return $this->belongsToMany(%s::class);', $relationShip));
                $methods[] = $method;
            });

        // hasMany
        collect($this->metaData['hasMany'])
            ->each(function ($relationShip) use (& $methods) {
                $method = new Method(Str::plural($relationShip['name']));
                $method->addBody(sprintf('return $this->hasMany(%s::class, \'%s\');', $relationShip['name'], $relationShip['foreign_id']));
                $methods[] = $method;
            });

        // belongsTo
        collect($this->metaData['belongsTo'])
            ->each(function ($relationShip) use (& $methods) {
                $method = new Method(Str::singular($relationShip['name']));
                $method->addBody(sprintf('return $this->belongsTo(%s::class, \'%s\');', $relationShip['name'], $relationShip['foreign_id']));
                $methods[] = $method;
            });

        // getters
        collect($this->metaData['appends'])
            ->each(function ($append) use (&$methods) {
                $method = new Method($append['getter']);
                $method->addBody(sprintf("return data_get(\$this->%s, '%s', %s);", $append['className'], $append['attribute'], $append['default']));

                $methods[] = $method;
            });

        return $methods;
    }
}
