<?php


namespace Tarre\Kickstarter\Generators\Models;


use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Str;
use Tarre\Kickstarter\KS\Abstracts\KSBase;
use Tarre\Kickstarter\KS\Argument;
use Tarre\Kickstarter\KS\Method;

/**
 * @property ModelGenerator model
 */
class PolicyGenerator extends KSBase
{
    protected $model;

    public function __construct(ModelGenerator $modelGenerator)
    {
        $this->model = $modelGenerator;

        $this->addUses($this->getModel()->getNamespaceClassname());
        $this->addUses(config('auth.providers.users.model'));
        $this->addUses(HandlesAuthorization::class);
        $this->addTrait(HandlesAuthorization::class);

    }

    public function getSavePath(): string
    {
        return config('laravel-kickstarter.models.policies.directory') . DIRECTORY_SEPARATOR . $this->getClassname() . '.php';
    }

    public function getNamespace(): string
    {
        return config('laravel-kickstarter.models.policies.namespace');
    }

    public function getClassname(): string
    {
        return $this->getModel()->getClassname() . 'Policy';
    }

    public function getMethods(): array
    {

        $methodCollection = collect(config('laravel-kickstarter.models.policies.events'))
            ->map(function ($passModel, $event) {

                $method = new Method($event);
                $method
                    ->addComment(sprintf('Determine whether the user can "%s" %s.', $event, strtolower(Str::plural($this->getModelNameAttribute()))))
                    ->addComment('@return boolean');

                // add body
                $method->addBody('return true;');

                // add user argument
                $method->addArgument((new Argument('user'))->setType(config('auth.providers.users.model')));

                // see
                if ($passModel) {
                    $method->addArgument((new Argument($this->getModelNameAttribute()))->setType($this->getModel()->getNamespaceClassname()));
                }

                return $method;
            });

        return $methodCollection->toArray();
    }

    public function getModel(): ModelGenerator
    {
        return $this->model;
    }

    protected function getModelNameAttribute()
    {
        return Str::camel(class_basename($this->getModel()->getNamespaceClassname()));
    }
}
