<?php


namespace Tarre\Kickstarter;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Publish necessary files
        $this->publishes([
            __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'laravel-kickstarter.php' => config_path('laravel-kickstarter.php'),
        ], 'laravel-kickstarter');

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        /* Register commands */
        $this->commands([
            Commands\MakeModels::class,
            Commands\MakeControllers::class,
            Commands\MakeRepositories::class,
            Commands\MakeFactories::class,
            Commands\MakeTests::class
            // UNDER DEV
        ]);
    }
}
