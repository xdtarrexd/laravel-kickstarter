<?php


namespace Tarre\Kickstarter;


use DB;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Nette\PhpGenerator\PhpFile;
use ReflectionClass;
use SplFileInfo;
use Tarre\Kickstarter\KS\Interfaces\KsArgument;
use Tarre\Kickstarter\KS\Interfaces\KsMethod;
use Tarre\Kickstarter\KS\Interfaces\KsProperty;
use Tarre\Kickstarter\KS\Interfaces\KsFile;

class Helper
{
    /**
     * @param AbstractPlatform $DBPlatform
     * @throws DBALException
     */
    public static function polyFillDoctrineTypes(AbstractPlatform $DBPlatform)
    {
        foreach (config('laravel-kickstarter.models.misc.doctrineMappings') as $dbType => $doctrineType) {
            if (!$DBPlatform->hasDoctrineTypeMappingFor($dbType)) {
                $DBPlatform->registerDoctrineTypeMapping($dbType, $doctrineType);
            }
        }
    }

    /**
     * @return Collection
     * @throws DBALException
     */
    public static function getConnectionTables($connection)
    {
        // Initialize the connection
        $DB = DB::connection($connection);
        $SchemaManager = $DB->getDoctrineSchemaManager();
        $DBPlatform = $SchemaManager->getDatabasePlatform();

        // Map custom doctrine data if needed
        self::polyFillDoctrineTypes($DBPlatform);

        // Prep all tables with necessary information to build first class models
        return collect($DB->getDoctrineSchemaManager()->listTableNames())
            // Filter out tables to ignore
            ->filter(function ($tableName) {
                return !in_array($tableName, config('laravel-kickstarter.models.ignore'));
            })
            ->map(function ($tableName) use ($SchemaManager) {

                $tableColumns = $SchemaManager->listTableColumns($tableName);
                $foreignKeys = $SchemaManager->listTableForeignKeys($tableName);


                $tableColumns = collect($tableColumns)
                    ->mapWithKeys(function ($val, $key) {
                        $newKey = preg_replace('/[^0-9_a-z-]/i', '', $key);
                        return [$newKey => $val];
                    });

                $foreignKeys = collect($foreignKeys)
                    ->mapWithKeys(function ($val, $key) {
                        $newKey = preg_replace('/[^0-9_a-z-]/i', '', $key);
                        return [$newKey => $val];
                    });

                return [
                    'tableName' => $tableName,
                    'className' => Str::studly(ucfirst(Str::singular($tableName))),
                    'columns' => $tableColumns,
                    'foreignKeys' => $foreignKeys
                ];
            })->values();
    }

    public static function findClassThatExtends($extension, $returnReflection = false): Collection
    {
        $files = File::allFiles(app_path());
        // luck all models
        return collect($files)
            // Create reflectionClass of files
            ->map(function (SplFileInfo $fileInfo) {
                $nameSpace = str_replace(app_path(), '', $fileInfo->getPath());
                $nameSpace = str_replace('/', '\\', $nameSpace);
                $nameSpace = 'App' . $nameSpace;
                $className = $fileInfo->getBasename('.php');
                $class = $nameSpace . '\\' . $className;

                return new ReflectionClass($class);
            })
            ->filter(function (ReflectionClass $class) use ($extension) {
                // Seek upwards until EloquentModel is found

                $parentClass = $class;

                while (true) {
                    if (!$parentClass) {
                        return false;
                    }

                    if ($parentClass->getName() == $extension) {
                        return true;
                    }
                    // No found? continue upwards
                    $parentClass = $parentClass->getParentClass();
                }

            })
            ->map(function (ReflectionClass $class) use ($returnReflection) {
                if ($returnReflection) {
                    return $class;
                }
                return $class->getName();
            })
            ->values();
    }

    public static function KsFileToNette(KsFile $ksFile): array
    {
        $netteFiles = [];

        // Build original class
        $netteFiles[$ksFile->getSavePath()] = self::HandleKsFile($ksFile);

        // Build their respective dependencies
        foreach ($ksFile->getAllDependencyFiles() as $dependency) {
            $netteFiles[$dependency->getSavePath()] = self::HandleKsFile($dependency);
        }

        return $netteFiles;
    }

    protected static function HandleKsFile(KsFile $ksFile): PhpFile
    {
        $file = new PhpFile();
        $namespace = $file->addNamespace($ksFile->getNamespace());
        if ($ksFile->isInterface()) {
            $class = $namespace->addInterface($ksFile->getClassname());
        } else {
            $class = $namespace->addClass($ksFile->getClassname());
        }

        // add implements
        collect($ksFile->implements())
            ->each(function ($implements) use ($class) {
                $class->addImplement($implements);
            });

        // add traits
        collect($ksFile->getTraits())
            ->each(function ($trait) use ($class) {
                $class->addTrait($trait);
            });
        // add class comments
        collect($ksFile->getComments())
            ->each(function ($comment) use ($class) {
                $class->addComment($comment);
            });

        // add extension if needed
        collect($ksFile->extends())
            ->each(function ($extends) use ($class) {
                $class->addExtend($extends);
            });

        // add class usages
        collect($ksFile->getUses())
            ->each(function (string $use) use ($namespace) {
                $namespace->addUse($use);
            });

        // add class properties
        collect($ksFile->getProperties())
            ->each(function (KsProperty $attribute) use ($class) {
                $class
                    ->addProperty($attribute->getName())
                    ->setVisibility($attribute->getVisibility())
                    ->setValue($attribute->getValue());
            });

        // add class methods
        collect($ksFile->getMethods())
            ->each(function (KsMethod $ksMethod) use ($class, $ksFile) {
                $method = $class->addMethod($ksMethod->getName());
                /*
                 * Set method type
                 */
                if (!empty($ksMethod->getType())) {
                    $method->setReturnType($ksMethod->getType());
                }

                /*
                 *  Add methods arguments
                 */
                collect($ksMethod->getArguments())
                    ->each(function (KsArgument $ksArgument) use ($method) {
                        $argument = $method->addParameter($ksArgument->getName());
                        $argument->setType($ksArgument->getType());
                        // TODO: Om man vill NUlla ändå?!
                        if (!is_null($ksArgument->getDefault())) {
                            $default = $ksArgument->getDefault() == 'null' ? null : $ksArgument->getDefault();
                            $argument->setDefaultValue($default);
                        }

                        // add params to comment
                        if (!empty($ksArgument->getType())) {
                            $method->addComment(sprintf('@param %s $%s', class_basename($ksArgument->getType()), $ksArgument->getName()));
                        } else {
                            $method->addComment(sprintf('@param $%s', $ksArgument->getName()));
                        }
                    });

                /*
                 * Add method custom comments
                 */
                collect($ksMethod->getComments())
                    ->each(function (string $comment) use ($method) {
                        $method->addComment($comment);
                    });

                /*
                 * add body
                 */
                if (!$ksFile->isInterface()) {
                    $method->setBody(implode(PHP_EOL, $ksMethod->getBody()));
                }

            });


        // and were done!
        return $file;
    }


}
