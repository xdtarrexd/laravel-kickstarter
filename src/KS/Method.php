<?php


namespace Tarre\Kickstarter\KS;

use Tarre\Kickstarter\KS\Interfaces\KsArgument;
use Tarre\Kickstarter\KS\Interfaces\KsMethod;
use Tarre\Kickstarter\KS\Traits\BodyTrait;
use Tarre\Kickstarter\KS\Traits\CommentsTrait;
use Tarre\Kickstarter\KS\Traits\ConstructNameTrait;
use Tarre\Kickstarter\KS\Traits\NameTrait;
use Tarre\Kickstarter\KS\Traits\TypeTrait;
use Tarre\Kickstarter\KS\Traits\VisibilityTrait;


class Method implements KsMethod
{
    use BodyTrait, VisibilityTrait, NameTrait, TypeTrait, CommentsTrait, ConstructNameTrait;

    protected $arguments = [];

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function setArguments(array $arguments): KsMethod
    {
        $this->arguments = [];
        foreach ($arguments as $argument) {
            $this->addArgument($argument);
        }
        return $this;
    }

    public function addArgument(KsArgument $argument): KsMethod
    {
        $this->arguments[] = $argument;
        return $this;
    }
}
