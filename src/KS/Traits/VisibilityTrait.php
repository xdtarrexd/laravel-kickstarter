<?php


namespace Tarre\Kickstarter\KS\Traits;


trait VisibilityTrait
{
    protected $visibility;

    /**
     * @return string
     */
    public function getVisibility(): string
    {
        return !empty($this->visibility) ? $this->visibility : 'public';
    }

    /**
     * @param string $visibility
     * @return $this
     */
    public function setVisibility(string $visibility): self
    {
        if (!in_array($visibility, ['public', 'protected', 'private'])) {
            throw new \InvalidArgumentException(sprintf('Unknown visibility "%s"', $visibility));
        }
        $this->visibility = $visibility;
        return $this;
    }
}
