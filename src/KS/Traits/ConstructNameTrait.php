<?php


namespace Tarre\Kickstarter\KS\Traits;


trait ConstructNameTrait
{
    /**
     * ConstructNameTrait constructor.
     * @param null $name
     */
    public function __construct($name = null)
    {
        if(!is_null($name)){
            $this->setName($name);
        }
    }

}
