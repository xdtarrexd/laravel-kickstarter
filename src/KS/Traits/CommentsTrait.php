<?php


namespace Tarre\Kickstarter\KS\Traits;


trait CommentsTrait
{
    protected $comments = [];

    public function getComments(): array
    {
        return $this->comments;
    }

    /**
     * @param array $comments
     * @return $this
     */
    public function setComments(array $comments): self
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * @param string $string
     * @return $this
     */
    public function addComment(string $string): self
    {
        $this->comments[] = $string;
        return $this;
    }
}
