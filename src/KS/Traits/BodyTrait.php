<?php

namespace Tarre\Kickstarter\KS\Traits;

trait BodyTrait
{
    protected $body = [];

    /**
     * @param array $body
     * @return $this
     */
    public function setBody(array $body): self
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function addBody(string $body = PHP_EOL): self
    {
        $this->body[] = $body;
        return $this;
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        return $this->body;
    }
}
