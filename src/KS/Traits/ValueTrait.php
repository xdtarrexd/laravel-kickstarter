<?php

namespace Tarre\Kickstarter\KS\Traits;

trait ValueTrait
{
    protected $value;

    /**
     * @param $mixed
     * @return $this
     */
    public function setValue($mixed): self
    {
        $this->value = $mixed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

}
