<?php


namespace Tarre\Kickstarter\KS\Traits;


trait TypeTrait
{
    protected $type = '';


    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

}
