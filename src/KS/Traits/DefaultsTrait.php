<?php


namespace Tarre\Kickstarter\KS\Traits;


use Tarre\Kickstarter\KS\Interfaces\KsArgument;

trait DefaultsTrait
{
    protected $default;

    /**
     * @param $mixed
     * @return $this
     */
    public function setDefault($mixed): self
    {
        $this->default = $mixed;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefault()
    {
        return $this->default;
    }
}
