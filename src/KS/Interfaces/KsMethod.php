<?php


namespace Tarre\Kickstarter\KS\Interfaces;


use Tarre\Kickstarter\KS\Interfaces\Misc\ConstructName;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasBody;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasComments;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasNames;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasTypes;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasVisibility;

interface KsMethod extends HasNames, HasVisibility, HasComments, HasTypes, HasBody, ConstructName
{
    public function addArgument(KsArgument $argument): self;

    public function setArguments(array $parameters): self;

    public function getArguments(): array;
}
