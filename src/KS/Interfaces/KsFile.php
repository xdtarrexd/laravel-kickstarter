<?php


namespace Tarre\Kickstarter\KS\Interfaces;


use Tarre\Kickstarter\KS\Interfaces\Misc\HasComments;

interface KsFile extends HasComments
{
    public function isInterface(): bool;

    public function getSavePath(): string; //.../etc/file.php

    public function getNamespace(): string; // Namespace

    public function getNamespaceClassname(): string; // Namespace/Name

    public function getClassname(): string; // Name

    public function getProperties(): array;

    public function getMethods(): array;

    public function extends(): array;

    public function implements(): array;

    // Common tasks

    public function getUses(): array;

    public function setUses(array $uses): self;

    public function addUses(string $usage): self;

    public function getTraits(): array;

    public function setTraits(array $traits): self;

    public function addTrait(string $trait): self;

    // Support for file in files
    public function getAllDependencyFiles(): array;

    public function getDependencyFile(string $alias): KsFile;

    public function addDependencyFile(string $alias, KsFile $ksFile, bool $replace = false): KsFile;

}
