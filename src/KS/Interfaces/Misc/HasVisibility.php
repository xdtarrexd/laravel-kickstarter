<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface HasVisibility
{
    /**
     * @return string
     */
    public function getVisibility(): string;

    /**
     * @param string $visibility
     * @return $this
     */
    public function setVisibility(string $visibility);
}
