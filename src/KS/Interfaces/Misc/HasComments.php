<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface HasComments
{
    /**
     * @return array
     */
    public function getComments(): array;

    /**
     * @param array $comments
     * @return $this
     */
    public function setComments(array $comments);

    /**
     * @param string $string
     * @return $this
     */
    public function addComment(string $string);

}
