<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface HasBody
{
    /**
     * @param array $body
     * @return $this
     */
    public function setBody(array $body);

    /**
     * @return array
     */
    public function getBody(): array;

    /**
     * @param string $body
     * @return $this
     */
    public function addBody(string $body = PHP_EOL);

}
