<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface HasValue
{
    /**
     * @param $mixed
     * @return $this
     */
    public function setValue($mixed);

    /**
     * @return mixed
     */
    public function getValue();
}
