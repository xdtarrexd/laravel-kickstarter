<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface ConstructName
{
    public function __construct($name = null);

}
