<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface HasDefaults
{
    /**
     * @param $mixed
     * @return $this
     */
    public function setDefault($mixed);

    /**
     * @return mixed
     */
    public function getDefault();
}
