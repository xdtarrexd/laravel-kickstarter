<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface HasNames
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name);
}
