<?php


namespace Tarre\Kickstarter\KS\Interfaces\Misc;


interface HasTypes
{
    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type);
}
