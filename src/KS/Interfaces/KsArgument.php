<?php


namespace Tarre\Kickstarter\KS\Interfaces;


use Tarre\Kickstarter\KS\Interfaces\Misc\HasNames;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasTypes;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasDefaults;
use Tarre\Kickstarter\KS\Interfaces\Misc\ConstructName;

interface KsArgument extends HasTypes, HasNames, HasDefaults, ConstructName
{

}
