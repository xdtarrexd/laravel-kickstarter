<?php


namespace Tarre\Kickstarter\KS\Interfaces;


use Tarre\Kickstarter\KS\Interfaces\Misc\ConstructName;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasNames;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasValue;
use Tarre\Kickstarter\KS\Interfaces\Misc\HasVisibility;

interface KsProperty extends  HasNames, HasVisibility, HasValue, ConstructName
{


}
