<?php


namespace Tarre\Kickstarter\KS;


use Tarre\Kickstarter\KS\Interfaces\KsProperty;
use Tarre\Kickstarter\KS\Traits\ConstructNameTrait;
use Tarre\Kickstarter\KS\Traits\NameTrait;
use Tarre\Kickstarter\KS\Traits\ValueTrait;
use Tarre\Kickstarter\KS\Traits\VisibilityTrait;

class Property implements KsProperty
{
    use VisibilityTrait, NameTrait, ValueTrait, ConstructNameTrait;
}
