<?php


namespace Tarre\Kickstarter\KS;


use Tarre\Kickstarter\KS\Interfaces\KsArgument;
use Tarre\Kickstarter\KS\Traits\DefaultsTrait;
use Tarre\Kickstarter\KS\Traits\NameTrait;
use Tarre\Kickstarter\KS\Traits\TypeTrait;


class Argument implements KsArgument
{
    use DefaultsTrait, NameTrait, TypeTrait;

    protected $name;

    public function __construct($name = null)
    {
        if (!is_null($name)) {
            $this->setName($name);
        }
    }

}
