<?php

namespace Tarre\Kickstarter\KS\Abstracts;

use Tarre\Kickstarter\KS\Interfaces\KsFile;
use Tarre\Kickstarter\KS\Traits\CommentsTrait;

abstract class KSBase implements KsFile
{
    use CommentsTrait;
    private $uses = [];
    private $traits = [];
    private $dependencies = [];

    /*
    public function __construct()
    {
        // Load dependencies for this KSFile
        $this->getDependencyFiles();
    }
    */

    public function isInterface(): bool
    {
        return false;
    }

    public function implements(): array
    {
        return [];
    }

    public function extends(): array
    {
        return [];
    }

    public function getProperties(): array
    {
        return [];
    }

    public function getNamespaceClassname(): string
    {
        return $this->getNamespace() . '\\' . $this->getClassname();
    }

    public function getAllDependencyFiles(): array
    {
        return $this->dependencies;
    }

    public function getDependencyFile(string $alias): KsFile
    {
        if (!isset($this->dependencies[$alias])) {
            throw new \InvalidArgumentException(sprintf('Alias "%s" not set', $alias));
        }
        return $this->dependencies[$alias];
    }

    public function addDependencyFile(string $alias, KsFile $ksFile, bool $replace = false): KsFile
    {
        if (isset($this->dependencies[$alias]) && !$replace) {
            throw new \InvalidArgumentException(sprintf('Alias "%s" is already set. Set $replace = true to override', $alias));
        }
        $this->dependencies[$alias] = $ksFile;
        return $ksFile;
    }

    /**
     * @return array
     */
    public function getUses(): array
    {
        return $this->uses;
    }

    /**
     * @param array $uses
     */
    public function setUses(array $uses): KsFile
    {
        $this->uses = [];
        foreach ($uses as $use) {
            $this->addUses($use);
        }
        return $this;
    }

    /**
     * @param string $usage
     */
    public function addUses(string $usage): KsFile
    {
        $this->uses[] = $usage;
        return $this;
    }

    /**
     * @return array
     */
    public function getTraits(): array
    {
        return $this->traits;
    }

    /**
     * @param array $traits
     * @return KsFile
     */
    public function setTraits(array $traits): KsFile
    {
        $this->traits = $traits;
        return $this;
    }

    public function addTrait(string $trait): KsFile
    {
        $this->traits[] = $trait;
        return $this;
    }
}
