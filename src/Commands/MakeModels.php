<?php


namespace Tarre\Kickstarter\Commands;


use Artisan;
use Doctrine\DBAL\DBALException;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Tarre\Kickstarter\Commands\Traits\CanAskForConnections;
use Tarre\Kickstarter\Commands\Traits\FileHandling;
use Tarre\Kickstarter\Generators\Models\ModelGenerator;
use Tarre\Kickstarter\Generators\Models\ModelStructureDetectiveLol;
use Tarre\Kickstarter\Generators\Models\ObserverGenerator;
use Tarre\Kickstarter\Generators\Models\ObserverServiceProviderGenerator;
use Tarre\Kickstarter\Generators\Models\PolicyGenerator;
use Tarre\Kickstarter\Generators\Models\PolicyServiceProviderGenerator;
use Tarre\Kickstarter\Helper;
use Tarre\Kickstarter\KS\Interfaces\KsFile;


class MakeModels extends Command
{
    use CanAskForConnections, FileHandling;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kickstart:models {--create-observers} {--create-policies} {--overwrite} {--ide-helper} {--connection=} {--filter=.*} {--ignore-providers}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates Models, policies and Observers from a given connection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {

        $connection = $this->askForConnections(true);

        try {
            $modelStructures = (new ModelStructureDetectiveLol($connection))->getClasses();
        } catch (DBALException $exception) {
            $this->warn(sprintf('Failed to parse connection "%s" due to an DBAL exception: %s', $connection, $exception->getMessage()));
            return null;
        } catch (Exception $exception) {
            $this->error(sprintf('Failed to parse connection "%s" due to an generic exception: %s', $connection, $exception->getMessage()));
            return null;
        }

        $this->info('# Creating models');
        $models = collect($modelStructures)
            ->map(function (array $collection) {
                return new ModelGenerator($collection);
            })
            ->filter(function(ModelGenerator $modelGenerator){
                return preg_match(sprintf('/%s/i', $this->option('filter')), $modelGenerator->getNamespaceClassname());
            })
            ->map(function (KsFile $ksFile) {
                return [
                    'phpFiles' => Helper::KsFileToNette($ksFile),
                    'ksFile' => $ksFile
                ];
            })
            ->map(function ($payload) {
                // create actual files
                $this->saveManyKsFiles($payload['phpFiles']);
                // files are created. Grab the classnames of our pairs
                return $payload['ksFile'];
            });

        // Run IDE helper if requested, and if any tables were created
        if ($this->option('ide-helper')) {
            $this->info('Attempting to run: php artisan ide-helper:models -RW');
            if (Artisan::call('ide-helper:models', [
                    '-R' => true,
                    '-W' => true
                ]) == 0) {
                $this->info('Success!');
            } else {
                $this->info('Failed!');
            }
        }

        if ($this->option('create-observers')) {
            $this->info('# Creating observers');
            $this->makeObservers($models);
        }

        if ($this->option('create-policies')) {
            $this->info('# Creating policies');
            $this->makePolicies($models);
        }
    }

    protected function makeObservers(Collection $models)
    {
        $createdObservers = $models
            ->map(function (ModelGenerator $modelGenerator) {
                return new ObserverGenerator($modelGenerator);
            })
            ->map(function (ObserverGenerator $ksFile) {
                return [
                    'phpFiles' => Helper::KsFileToNette($ksFile),
                    'ksFile' => $ksFile
                ];
            })
            ->map(function ($payload) {
                $this->saveManyKsFiles($payload['phpFiles']);
                return $payload['ksFile'];
            });


        if ($createdObservers->count() > 0 && !$this->option('ignore-providers')) {
            $observerPairs = $createdObservers->map(function (ObserverGenerator $observer) {
                return [
                    'model' => $observer->getModel()->getNamespaceClassname(),
                    'observer' => $observer->getNamespaceClassname()
                ];
            });

            $ksProvider = new ObserverServiceProviderGenerator($observerPairs);
            $phpFiles = Helper::KsFileToNette($ksProvider);
            $this->saveManyKsFiles($phpFiles, false);
        }
    }

    protected function makePolicies(Collection $models)
    {
        $createdPolicies = $models
            ->map(function (ModelGenerator $modelGenerator) {
                return new PolicyGenerator($modelGenerator);
            })
            ->map(function (PolicyGenerator $ksFile) {
                return [
                    'phpFiles' => Helper::KsFileToNette($ksFile),
                    'ksFile' => $ksFile
                ];
            })
            ->map(function ($payload) {
                $this->saveManyKsFiles($payload['phpFiles']);
                return $payload['ksFile'];
            });

        if ($createdPolicies->count() > 0 && !$this->option('ignore-providers')) {
            $policyPairs = $createdPolicies->map(function (PolicyGenerator $policy) {
                return [
                    'model' => $policy->getModel()->getNamespaceClassname(),
                    'policy' => $policy->getNamespaceClassname()
                ];
            });

            $ksProvider = new PolicyServiceProviderGenerator($policyPairs);
            $phpFiles = Helper::KsFileToNette($ksProvider);
            $this->saveManyKsFiles($phpFiles, false, '',true);
        }
    }
}
