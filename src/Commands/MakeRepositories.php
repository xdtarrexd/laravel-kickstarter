<?php


namespace Tarre\Kickstarter\Commands;


use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\Printer;
use Tarre\Kickstarter\Commands\Traits\CanAskForConnections;
use Tarre\Kickstarter\Commands\Traits\FileHandling;
use Tarre\Kickstarter\Generators\Repository\EloquentGenerator;
use Tarre\Kickstarter\Generators\Repository\ServiceProviderGenerator;
use Tarre\Kickstarter\Helper;
use Tarre\Kickstarter\KS\Interfaces\KsFile;

/**
 * @property Printer printer
 */
class MakeRepositories extends Command
{
    use CanAskForConnections, FileHandling;
    protected $printer;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kickstart:repositories {--overwrite} {--filter=.*} {--ignore-providers}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates repositories from all existing Models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->printer = new Printer;

        $classes = Helper::findClassThatExtends(Model::class);

        // Find all models in app Directory
        $repositories = $classes
            ->map(function (string $model) {
                return new EloquentGenerator($model);
            })
            ->filter(function (EloquentGenerator $eloquentGenerator) {
                return preg_match(sprintf('/%s/i', $this->option('filter')), $eloquentGenerator->getNamespaceClassname());
            })
            ->map(function (KsFile $ksFile) {
                return [
                    'phpFiles' => Helper::KsFileToNette($ksFile),
                    'ksFile' => $ksFile
                ];
            })->map(function (array $payload) {
                // create actual files
                $this->saveManyKsFiles($payload['phpFiles']);
                // files are created. Grab the classnames of our pairs
                return $payload['ksFile'];
            })
            ->map(function (KsFile $ksFile) {
                $eloquent = $ksFile->getNamespaceClassname();
                $repository = $ksFile->getDependencyFile('interface')->getNamespaceClassname();
                return [
                    'class' => $eloquent,
                    'interface' => $repository
                ];
            });

        // create provider file
        if (!$this->option('ignore-providers')) {
            $serviceProvider = new ServiceProviderGenerator($repositories);
            $phpFiles = Helper::KsFileToNette($serviceProvider);
            $this->saveManyKsFiles($phpFiles, false);
        }

    }
}
