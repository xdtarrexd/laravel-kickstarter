<?php


namespace Tarre\Kickstarter\Commands;


use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use ReflectionClass;
use Tarre\Kickstarter\Commands\Traits\CanAskForConnections;
use Tarre\Kickstarter\Commands\Traits\FileHandling;
use Tarre\Kickstarter\Generators\Factory\FactoryGenerator;
use Tarre\Kickstarter\Generators\Factory\FactoryStructureDetectiveLol;
use Tarre\Kickstarter\Helper;

class MakeFactories extends Command
{
    use FileHandling, CanAskForConnections;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kickstart:factories {--overwrite} {--filter=.*} {--connection=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates factories from all existing Models';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $connections = $this->askForConnections();

        Helper::findClassThatExtends(Model::class, true)
            ->map(function (ReflectionClass $reflectionClass) use ($connections) {
                return (new FactoryStructureDetectiveLol)->resolve($reflectionClass, $connections);
            })
            ->filter(function ($pair) {
                return is_array($pair);
            })
            ->map(function (array $pair) {
                return new FactoryGenerator($pair);
            })
            ->filter(function (FactoryGenerator $factoryGenerator) {
                return preg_match(sprintf('/%s/i', $this->option('filter')), $factoryGenerator->getSavePath());
            })
            ->map(function (FactoryGenerator $factoryGenerator) {
                return [
                    'savePath' => $factoryGenerator->getSavePath(),
                    'content' => $factoryGenerator->getContent()
                ];
            })
            ->each(function (array $file) {
                if ($this->protectFile($file['savePath']) && !$this->option('overwrite')) {
                    return;
                }
                $this->saveFile($file['savePath'], $file['content']);
            });
    }
}
