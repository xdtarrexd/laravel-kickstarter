<?php


namespace Tarre\Kickstarter\Commands\Traits;


trait CanAskForConnections
{
    /**
     * @return mixed
     */
    public function askForConnections($singleSelect = false)
    {
        if ($this->hasOption('connection') && $this->option('connection')) {
            return $singleSelect ? $this->option('connection') : [$this->option('connection')];
        }
        $possibleConnections = config('laravel-kickstarter.models.connections');

        if ($singleSelect) {
            $moreOptions = [];
        } else {
            $moreOptions = ['Everything listed below'];
        }
        $connection = $this->choice('Which connection do you wish to parse?', array_merge($moreOptions, $possibleConnections));

        if ($connection === 'Everything listed below') {
            $connections = $possibleConnections;
        } else {
            $connections = [$connection];
        }

        return $singleSelect ? $connections[0] : $connections;
    }

}
