<?php


namespace Tarre\Kickstarter\Commands\Traits;


use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\Printer;

trait FileHandling
{

    public function saveFile($file, $content, $info = '', $removeQuotes = false)
    {
        $dirName = dirname($file);
        if (!file_exists($dirName)) {
            mkdir($dirName, 0775, true);
        }

        //
        if($removeQuotes){
            $content = preg_replace("/'/", '', $content);
        }

        file_put_contents($file, $content);
        $this->info(sprintf('The file "%s" has been successfully created!%s', $file, $info));
    }

    public function protectFile($file)
    {
        if (file_exists($file) && !$this->option('overwrite')) {
            $this->warn(sprintf('The file "%s" already exists use --overwrite to ignore warning', $file));
            return true; // "Yes, protect the file"
        }
        return false; // "No, dont protect the file"
    }

    public function saveManyKsFiles(array $phpFiles, $protectFiles = true, $info = '', $removeQuotes = false)
    {
        static $printer;
        if (!$printer) {
            $printer = new Printer;
        }

        collect($phpFiles)
            ->filter(function (PhpFile $phpFile, $savePath) use ($protectFiles) {
                if (!$protectFiles) {
                    return true;
                }
                // protect files
                return $this->protectFile($savePath) == false;
            })
            ->each(function (PhpFile $phpFile, $savePath) use ($printer, $info, $removeQuotes) {
                $this->saveFile($savePath, $printer->printFile($phpFile), $info, $removeQuotes);
            });
    }

}
