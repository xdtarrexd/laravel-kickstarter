<?php


namespace Tarre\Kickstarter\Commands;

use Artisan;
use DB;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Nette\PhpGenerator\PhpFile;
use Nette\PhpGenerator\Printer;
use ReflectionClass;
use Tarre\Kickstarter\Commands\Traits\CanAskForConnections;
use Tarre\Kickstarter\Commands\Traits\FileHandling;
use Tarre\Kickstarter\Generators\Controller\ControllerGenerator;
use Tarre\Kickstarter\Helper;
use Tarre\Kickstarter\KS\Interfaces\KsFile;
use Tarre\Kickstarter\KS\Interfaces\KsMethod;

/**
 * @property Collection models
 */
class MakeControllers extends Command
{
    use CanAskForConnections, FileHandling;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kickstart:controllers {--use-policies} {--overwrite} {--filter=.*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates controllers, requests and routes from all existing repositories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \Doctrine\DBAL\DBALException
     */
    public function handle()
    {
        // Polyfill doctrine types
        Helper::polyFillDoctrineTypes(DB::connection()->getDoctrineSchemaManager()->getDatabasePlatform());
        $globalRoutes = [];

        $this->allInterfaces()
            ->map(function (ReflectionClass $repository) {
                // Based on all interfaces. Start creating controllers
                return new ControllerGenerator($repository, $this->option('use-policies'));
            })
            ->each(function (ControllerGenerator $controller) use (&$globalRoutes) {
                // Build routes
                // TODO felt lazy, might improve later..
                $globalRoutes[] = $this->createRoutes($controller);
            })
            ->filter(function(ControllerGenerator $controllerGenerator){
                return preg_match(sprintf('/%s/i', $this->option('filter')), $controllerGenerator->getNamespaceClassname());
            })
            ->map(function (KsFile $ksFile) {
                // Convert to nette file
                return Helper::KsFileToNette($ksFile);
            })
            ->each(function (array $ksFiles) {
                $this->saveManyKsFiles($ksFiles);
            });

        // Now when all files are created. We finish up routes
        $routeData = collect($globalRoutes)->flatten()->implode(PHP_EOL);

        $routesPath = base_path() . DIRECTORY_SEPARATOR . 'routes' . DIRECTORY_SEPARATOR . 'api.php';
        file_put_contents($routesPath, '/*' . PHP_EOL, FILE_APPEND);
        file_put_contents($routesPath, $routeData, FILE_APPEND);
        file_put_contents($routesPath, PHP_EOL . '*/', FILE_APPEND);
        $this->info(sprintf('Appended data to "%s" BUT ITS COMMENTED. DONT FORGET TO REMOVE THEM!', $routesPath));
    }


    protected function allInterfaces(): Collection
    {
        $providerReflection = $this->getProviderReflection();

        $contents = file_get_contents($providerReflection->getFileName());

        if (preg_match_all('/use ([^;]+)/m', $contents, $uses)) {
            $uses = $uses[1];
        } else {
            $uses = [];
        }

        if (preg_match_all('/bind\(([^,]+)/m', $contents, $binds)) {
            $binds = $binds[1];
        } else {
            $binds = [];
        }

        return $this->getInterfaceReflections($uses, $binds);
    }

    protected function getInterfaceReflections($uses, $binds): Collection
    {
        return collect($binds)
            ->map(function ($interface) use ($uses) {
                foreach ($uses as $use) {
                    $useFqdn = sprintf('%s::class', $use);

                    if (Str::contains($useFqdn, $interface)) {
                        return $use;
                    }
                }
            })
            ->map(function ($useFqdn) {
                return new ReflectionClass($useFqdn);
            });
    }

    protected function getProviderReflection(): ReflectionClass
    {
        $repositoryFileClass = Helper::findClassThatExtends(ServiceProvider::class, true)
            ->filter(function (ReflectionClass $reflection) {
                return $reflection->getName() == config('laravel-kickstarter.controllers.repositoryProviderNamespace');
            })->first();

        if (!$repositoryFileClass) {
            $this->error('Could not find repository definitions in the given provider. Please check your config!');
            throw new \Exception('Could not find repository definitions in the given provider. Please check your config!');
        }

        return $repositoryFileClass;
    }


    // TODO: improve this dogshit

    protected function createRoutes(ControllerGenerator $controller): array
    {
        $modelName = $controller->guessModelNameFromRepositoryName();
        $controllerName = class_basename($controller->getNamespaceClassname());
        $baseUri = Str::kebab(Str::plural($modelName));
        $routes[] = sprintf("Route::apiResource('%s', '%s');",
            $baseUri,
            $controllerName);
        $routes[] = sprintf("Route::prefix('%s/{id}')->group(function () {", $baseUri);
        collect($controller->getMethods())
            ->filter(function (KsMethod $ksMethod) {
                // TODO Same issue here as in the attribute "$ignoreTheseMethodNames" at ControllerGenerator.php
                return !in_array(strtolower($ksMethod->getName()), [
                    '__construct',
                    'index',
                    'update',
                    'store',
                    'destroy',
                    'show'
                ]);
            })
            ->each(function (KsMethod $ksMethod) use (&$routes, $controllerName, $baseUri, $modelName) {
                // See if we can make some educated guesses
                if (preg_match('/^(get|put|post)([A-Za-z]+)/', $ksMethod->getName(), $m)) {
                    $method = $m[1];
                    $uri = Str::plural($m[2]);
                } else {
                    $method = 'any';
                    $uri = $ksMethod->getName();
                }

                // cuteify stuff like "modelName-something-cool" would be better of named "something-cool"
                $uri = preg_replace("/" . strtolower($modelName) . "$/i", '', $uri);

                // snake and friendly the url
                $uri = Str::snake($uri);
                $uri = Str::slug($uri);

                // LC
                $uri = strtolower($uri);

                $routes[] = sprintf("    Route::%s('%s', '%s@%s');",
                    $method,
                    $uri,
                    $controllerName,
                    $ksMethod->getName());
            });
        $routes[] = '});' . PHP_EOL;

        return $routes;
    }

}
