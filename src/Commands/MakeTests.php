<?php


namespace Tarre\Kickstarter\Commands;


use Illuminate\Console\Command;
use Illuminate\Routing\Route;
use Illuminate\Support\Collection;
use Tarre\Kickstarter\Commands\Traits\FileHandling;
use Tarre\Kickstarter\Generators\Test\TestGenerator;
use Tarre\Kickstarter\Generators\Test\TestGeneratorMethodStubs;
use Tarre\Kickstarter\Helper;

class MakeTests extends Command
{
    use FileHandling;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kickstart:tests {--overwrite} {--filter=.*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates tests based on what is present in route:print';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        collect(app('router')->getRoutes())
            ->filter(function (Route $route) {
                // From all defined routes. Ignore closure
                return $route->getActionName() !== 'Closure';
            })
            ->map(function (Route $route) {
                // from the routes. Generate some test stubs
                try {
                    return (new TestGeneratorMethodStubs($route));
                } catch (\Exception $exception) {
                    $this->warn(sprintf('failed to parse route "%s": %s', $route->uri(), $exception->getMessage()));
                    return null;
                }
            })
            // ignore failed parsed controllers
            ->reject(null)
            ->groupBy(function (TestGeneratorMethodStubs $stubs) {
                // Group all stubs by their save path
                return $stubs->getSavePath();
            })
            ->map(function (Collection $stubs, $path) {
                // from the grouped stubs. flatten them
                $methods = $stubs->map(function (TestGeneratorMethodStubs $stubs) {
                    return $stubs->getMethods();
                })
                    ->flatten(1)->toArray();

                // Create KS file
                return new TestGenerator($methods, $path);
            })
            ->filter(function(TestGenerator $testGenerator){
                return preg_match(sprintf('/%s/i', $this->option('filter')), $testGenerator->getNamespaceClassname());
            })
            ->map(function (TestGenerator $ksFile) {
                // Create actual file
                return [
                    'phpFiles' => Helper::KsFileToNette($ksFile),
                    'ksFile' => $ksFile
                ];
            })
            ->each(function (array $payload) {
                // save
                $this->saveManyKsFiles($payload['phpFiles']);
            });
    }
}
