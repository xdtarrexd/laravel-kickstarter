#### Migrations

```php
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('active');
            $table->json('payload');
            $table->timestamps();
        });
        
        Schema::create('keywords', function(Blueprint $table){
            $table->increments('id');
        });
        
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->unsignedInteger('company_id')->index();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::create('roles', function(Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        // pivot table for roles and employees
        Schema::create('employee_role', function(Blueprint $table){
            $table->unsignedInteger('role_id')->index();
            $table->unsignedInteger('employee_id')->index();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('employee_id')->references('id')->on('employees');
        });

        // pivot table for companies and keywords
        Schema::create('company_keyword', function(Blueprint $table){
            $table->unsignedInteger('company_id')->index();
            $table->unsignedInteger('keyword_id')->index();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('keyword_id')->references('id')->on('keywords');
        });
```

### This example will only follow how Company is setup

#### `app\Models\Company.php`

```php
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	public $table = 'companies';

	public $primaryKey = 'id';

	public $fillable = ['name', 'active', 'payload'];

	public $casts = ['name' => 'string', 'active' => 'boolean', 'payload' => 'array'];

	public $hidden = ['Employees', 'Keywords'];

	public $appends = [];


	public function Keywords()
	{
		return $this->belongsToMany(Keyword::class);
	}


	public function Employees()
	{
		return $this->hasMany(Employee::class);
	}
}
```

#### `app\Policies\CompanyPolicy.php`

```php
<?php

namespace App\Policies;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
	use HandlesAuthorization;

	/**
	 * @param Employee $user
	 * Determine whether the user can "viewAny" companies.
	 * @return boolean
	 */
	public function viewAny(Employee $user)
	{
		return true;
	}


	/**
	 * @param Employee $user
	 * @param Company $company
	 * Determine whether the user can "view" companies.
	 * @return boolean
	 */
	public function view(Employee $user, Company $company)
	{
		return true;
	}


	/**
	 * @param Employee $user
	 * Determine whether the user can "create" companies.
	 * @return boolean
	 */
	public function create(Employee $user)
	{
		return true;
	}


	/**
	 * @param Employee $user
	 * @param Company $company
	 * Determine whether the user can "update" companies.
	 * @return boolean
	 */
	public function update(Employee $user, Company $company)
	{
		return true;
	}


	/**
	 * @param Employee $user
	 * @param Company $company
	 * Determine whether the user can "delete" companies.
	 * @return boolean
	 */
	public function delete(Employee $user, Company $company)
	{
		return true;
	}


	/**
	 * @param Employee $user
	 * @param Company $company
	 * Determine whether the user can "restore" companies.
	 * @return boolean
	 */
	public function restore(Employee $user, Company $company)
	{
		return true;
	}


	/**
	 * @param Employee $user
	 * @param Company $company
	 * Determine whether the user can "forceDelete" companies.
	 * @return boolean
	 */
	public function forceDelete(Employee $user, Company $company)
	{
		return true;
	}
}
```

#### `app\Policies\CompanyObserver.php`

```php
<?php

namespace App\Observers;

use App\Models\Company;

class CompanyObserver
{
	/**
	 * @param Company $company
	 * Handle the company "retrieved" event
	 */
	public function retrieved(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "creating" event
	 */
	public function creating(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "created" event
	 */
	public function created(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "updating" event
	 */
	public function updating(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "updated" event
	 */
	public function updated(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "saving" event
	 */
	public function saving(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "saved" event
	 */
	public function saved(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "deleting" event
	 */
	public function deleting(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "deleted" event
	 */
	public function deleted(Company $company)
	{
		//
	}


	/**
	 * @param Company $company
	 * Handle the company "restored" event
	 */
	public function restored(Company $company)
	{
		//
	}
}
```

#### `app\Repositories\Company\CompanyRepository.php`

```php
<?php

namespace App\Repositories\Company;

use App\Models\Company;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface CompanyRepository
{
	/**
	 * @param $search
	 * @param $filter
	 * @return LengthAwarePaginator
	 */
	function index($search = '', $filter = null): LengthAwarePaginator;


	/**
	 * @param $companyId
	 * @return Company
	 */
	function findOrFail($companyId): Company;


	/**
	 * @param array $attributes
	 * @return Company
	 */
	function store(array $attributes): Company;


	/**
	 * @param array $attributes
	 * @param $companyId
	 * @return Company
	 */
	function update(array $attributes, $companyId): Company;


	/**
	 * @param $companyId
	 * @param $forceDelete
	 * @return Company
	 * @throws Exception
	 */
	function destroyOrFail($companyId, $forceDelete = false): Company;


	/**
	 * @param $companyId
	 * @param $search
	 * @param $filter
	 * @return LengthAwarePaginator
	 */
	function getKeywords($companyId, $search = '', $filter = null): LengthAwarePaginator;


	/**
	 * @param $companyId
	 * @param $search
	 * @param $filter
	 * @return LengthAwarePaginator
	 */
	function getEmployees($companyId, $search = '', $filter = null): LengthAwarePaginator;
}
```

#### `app\Repositories\Company\EloquentCompany.php`

```php
<?php

namespace App\Repositories\Company;

use App\Models\Company;
use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EloquentCompany implements CompanyRepository
{
	/**
	 * @param $search
	 * @param $filter
	 * @return LengthAwarePaginator
	 */
	public function index($search = '', $filter = null): LengthAwarePaginator
	{
		return Company::paginate(25)->appends('search', $search);
	}


	/**
	 * @param $companyId
	 * @return Company
	 */
	public function findOrFail($companyId): Company
	{
		return Company::whereId($companyId)->firstOrFail();
	}


	/**
	 * @param array $attributes
	 * @return Company
	 */
	public function store(array $attributes): Company
	{
		$company = new Company;
		$company->fill($attributes);
		$company->save();
		return $company;
	}


	/**
	 * @param array $attributes
	 * @param $companyId
	 * @return Company
	 */
	public function update(array $attributes, $companyId): Company
	{
		$company = $this->findOrFail($companyId);
		$company->fill($attributes);
		$company->save();
		return $company;
	}


	/**
	 * @param $companyId
	 * @param $forceDelete
	 * @return Company
	 * @throws Exception
	 */
	public function destroyOrFail($companyId, $forceDelete = false): Company
	{
		$company = $this->findOrFail($companyId);
		if ($forceDelete) {
		    $company->forceDelete();
		} else {
		    $company->delete();
		}
		return $company;
	}


	/**
	 * @param $companyId
	 * @param $search
	 * @param $filter
	 * @return LengthAwarePaginator
	 */
	public function getKeywords($companyId, $search = '', $filter = null): LengthAwarePaginator
	{
		$company = $this->findOrFail($companyId);
		return $company->Keywords()->paginate(25)->appends('search', $search);
	}


	/**
	 * @param $companyId
	 * @param $search
	 * @param $filter
	 * @return LengthAwarePaginator
	 */
	public function getEmployees($companyId, $search = '', $filter = null): LengthAwarePaginator
	{
		$company = $this->findOrFail($companyId);
		return $company->Employees()->paginate(25)->appends('search', $search);
	}
}
```

#### `app\Http\Controllers\CompanyController.php`

```php
<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCompanyRequest;
use App\Models\Company;
use App\Repositories\Company\CompanyRepository;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Request;

/**
 * @property CompanyRepository companyRepository
 */
class CompanyController extends Controller
{
	protected $companyRepository;


	/**
	 * @param CompanyRepository $companyRepository
	 */
	public function __construct(CompanyRepository $companyRepository)
	{
		$this->companyRepository = $companyRepository;
	}


	/**
	 * @throws AuthorizationException
	 * @return LengthAwarePaginator
	 */
	public function index()
	{
		$this->authorize('viewAny', Company::class);
		$search = Request::get('search', '');
		$filter = Request::get('filter');
		return $this->companyRepository->index($search, $filter);
	}


	/**
	 * @param $id
	 * @throws AuthorizationException
	 * @return Company
	 */
	public function show($id)
	{
		$company = $this->companyRepository->findOrFail($id);
		$this->authorize('view', $company);
		return $company;
	}


	/**
	 * @param StoreCompanyRequest $request
	 * @throws AuthorizationException
	 * @return Company
	 */
	public function store(StoreCompanyRequest $request)
	{
		$this->authorize('create', Company::class);
		return $this->companyRepository->store($request->all());
	}


	/**
	 * @param StoreCompanyRequest $request
	 * @param $id
	 * @throws AuthorizationException
	 * @return Company
	 */
	public function update(StoreCompanyRequest $request, $id)
	{
		$company = $this->companyRepository->findOrFail($id);
		$this->authorize('update', $company);
		return $this->companyRepository->update($request->all(), $id);
	}


	/**
	 * @param $id
	 * @throws AuthorizationException
	 * @throws Exception
	 * @return Company
	 */
	public function destroy($id)
	{
		$company = $this->companyRepository->findOrFail($id);
		$this->authorize('delete', $company);
		return $this->companyRepository->destroyOrFail($id);
	}


	/**
	 * @param $id
	 * @throws AuthorizationException
	 * @return LengthAwarePaginator
	 */
	public function getKeywords($id)
	{
		$company = $this->companyRepository->findOrFail($id);
		$this->authorize('view', $company);
		$search = Request::get('search', '');
		$filter = Request::get('filter');
		return $this->companyRepository->getKeywords($search, $filter);
	}


	/**
	 * @param $id
	 * @throws AuthorizationException
	 * @return LengthAwarePaginator
	 */
	public function getEmployees($id)
	{
		$company = $this->companyRepository->findOrFail($id);
		$this->authorize('view', $company);
		$search = Request::get('search', '');
		$filter = Request::get('filter');
		return $this->companyRepository->getEmployees($search, $filter);
	}
}
```

#### `app\Http\Controllers\Requests\StoreCompanyRequest.php`

```php
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCompanyRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}


	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
		    'name' => ['required', 'string'],
		    'active' => ['required', 'boolean'],
		    'payload' => ['required', 'array']
		];
	}


	public function messages()
	{
		// Remove comments to return a custom message
		return [
		    //'name.required' => '',
		    //'name.string' => '',
		    //'active.required' => '',
		    //'active.boolean' => '',
		    //'payload.required' => '',
		    //'payload.array' => ''
		];
	}
}
```

#### `CompanyFactory.php`

```php
<?php

/**
 * @var \Illuminate\Database\Eloquent\Factory $factory
 */

use App\Models\Company;
use Faker\Generator as Faker;

$factory->define(Company::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'active' => $faker->boolean,
        'payload' => [],
        'created_at' => $faker->datetime(),
        'updated_at' => $faker->datetime(),
    ];
});
```
#### `CompanyControllerTest.php`

```php
<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class CompanyControllerTest extends TestCase
{
	use RefreshDatabase;

	public function test_index_with_success()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('GET', config('app.url') . sprintf('/api/companies'));

		$response->assertStatus(Response::HTTP_OK);
	}


	public function test_store_with_failure()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('POST', config('app.url') . sprintf('/api/companies'), [
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}


	public function test_store_with_success()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('POST', config('app.url') . sprintf('/api/companies'), [
		    'name' => 'Zechariah Wiegand',
		    'active' => true,
		    'payload' => [],
		]);

		$response->assertStatus(Response::HTTP_CREATED);
	}


	public function test_show_with_failure()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('GET', config('app.url') . sprintf('/api/companies/{company}'));

		$response->assertStatus(Response::HTTP_NOT_FOUND);
	}


	public function test_show_with_success()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('GET', config('app.url') . sprintf('/api/companies/{company}'));

		$response->assertStatus(Response::HTTP_OK);
	}


	public function test_update_with_failure()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('PUT', config('app.url') . sprintf('/api/companies/{company}'), [
		]);

		$response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
	}


	public function test_update_with_success()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('PUT', config('app.url') . sprintf('/api/companies/{company}'), [
		    'name' => 'Sydni Bartell',
		    'active' => true,
		    'payload' => [],
		]);

		$response->assertStatus(Response::HTTP_OK);
	}


	public function test_destroy_with_failure()
	{
		$response = $this->json('DELETE', config('app.url') . sprintf('/api/companies/{company}'));

		$response->assertStatus(Response::HTTP_NOT_FOUND);
	}


	public function test_destroy_with_success()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('DELETE', config('app.url') . sprintf('/api/companies/{company}'));

		$response->assertStatus(Response::HTTP_OK);
	}


	public function test_get_keywords_with_failure()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('GET', config('app.url') . sprintf('/api/companies/{id}/keywords'));

		$response->assertStatus(Response::HTTP_NOT_FOUND);
	}


	public function test_get_keywords_with_success()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('GET', config('app.url') . sprintf('/api/companies/{id}/keywords'));

		$response->assertStatus(Response::HTTP_OK);
	}


	public function test_get_employees_with_failure()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('GET', config('app.url') . sprintf('/api/companies/{id}/employees'));

		$response->assertStatus(Response::HTTP_NOT_FOUND);
	}


	public function test_get_employees_with_success()
	{
		echo "Validate test and remove these 2 lines";
		return;
		$response = $this->json('GET', config('app.url') . sprintf('/api/companies/{id}/employees'));

		$response->assertStatus(Response::HTTP_OK);
	}
}
```
