### Docs

* [Code example](example.md)
* [Read more about generating models](models.md)
* [Read more about generating factories](factories.md)
* [Read more about generating repositories](repositories.md)
* [Read more about generating controllers](controllers.md)
* [Read more about generating tests](tests.md)
